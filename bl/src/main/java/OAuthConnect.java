/**
 * Created by maktareq on 02.04.17.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class OAuthConnect {

   public static void main(String[] args) {
      try {
         HttpClient httpclient = HttpClientBuilder.create().build();  // the http-client, that will send the request
         HttpGet httpGet = new HttpGet("http://xploit.ibmt.fraunhofer.de:8080/portal/rest/semanticRepository/query?search=mating");   // the http GET request
         httpGet.addHeader("Authorization", "Bearer fa4f399b-bf27-429e-8e07-6353dd990ea5"); // add the authorization header to the request
         HttpResponse response = httpclient.execute(httpGet);
         System.out.println (response);// the client executes the request and gets a response
         int responseCode = response.getStatusLine().getStatusCode();  // check the response code
         BufferedReader rd = new BufferedReader(
            new InputStreamReader(response.getEntity().getContent()));

         StringBuffer result = new StringBuffer();
         String line = "";
         while ((line = rd.readLine()) != null) {
            result.append(line);
         }
         System.out.println(result);
         switch (responseCode) {
            case 200: {
               // everything is fine, handle the response
               String stringResponse = EntityUtils.toString(response.getEntity());  // now you have the response as String, which you can convert to a JSONObject or do other stuff
               break;
            }
            case 500: {
               // server problems ?
               break;
            }
            case 403: {
               // you have no authorization to access that resource
               break;
            }
         }
      } catch (IOException ex) {
         // handle exception
      }
   }
}
