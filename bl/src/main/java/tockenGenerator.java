import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Element;


/**
 * Created by maktareq on 02.04.17.
 */
public class tockenGenerator {


   static final String REST_URL = "http://xploit.ibmt.fraunhofer.de:8080/authorization-service/oauth/token?grant_type=password&username=s8mdkadi&password=CZ094gh401818898982!";
   static final String RDF_URL="http://xploit.ibmt.fraunhofer.de:8080/portal/rest/semanticRepository/query?search=mating";
   static String bearerTocken="";
   public static void main(String[]args) {



      bearerTocken=getTocken(REST_URL);
      spaceRemover();

      //System.out.println(bearerTocken);
     // String loadrdf=getRDFXML("Tareque",bearerTocken);
      //System.out.println(loadrdf);
      listModifier();

   }
// XML generator
   private static String getRDFXML(String urlToGet, String tocken) {
      StringBuffer result = new StringBuffer();

      try {
         HttpClient httpclient = HttpClientBuilder.create().build();
         // the http-client, that will send the request
         HttpGet httpGet = new HttpGet("http://xploit.ibmt.fraunhofer.de:8080/portal/rest/semanticRepository/query?search="+urlToGet);   // the http GET request

         httpGet.addHeader("Authorization", "Bearer "+ tocken); // add the authorization header to the request
         HttpResponse response = httpclient.execute(httpGet);
         //System.out.println (response);// the client executes the request and gets a response
         int responseCode = response.getStatusLine().getStatusCode();  // check the response code
         BufferedReader rd = new BufferedReader(
            new InputStreamReader(response.getEntity().getContent()));


         String line = "";
         while ((line = rd.readLine()) != null) {
            result.append(line);
         }
         //System.out.println(result);


         switch (responseCode) {
            case 200: {
               // everything is fine, handle the response
               String stringResponse = EntityUtils.toString(response.getEntity());  // now you have the response as String, which you can convert to a JSONObject or do other stuff
               break;
            }
            case 500: {
               // server problems ?
               break;
            }
            case 403: {
               // you have no authorization to access that resource
               break;
            }
         }
      } catch (IOException ex) {
         // handle exception
      }

      return result.toString();
   }


   //Tocken Generator


   private static String getTocken(String urlToGet) {
      URL url;
      HttpURLConnection conn;
      BufferedReader rd;
      String line;
      String result = "";
      String userCredentials = "xploit-trusted-client:Xpl0it!";
      String basicAuth = "Basic " + new String(new Base64().encode(userCredentials.getBytes()));
      try {
         url = new URL(urlToGet);
         conn = (HttpURLConnection) url.openConnection();
         conn.setRequestMethod("POST");
         conn.setRequestProperty("Authorization", basicAuth);
         conn.setRequestProperty("Accept", "application/xml");
         rd = new BufferedReader(
            new InputStreamReader(conn.getInputStream()));
         while ((line = rd.readLine()) != null) {

            result += line;
         }
         rd.close();
      } catch (Exception e) {
         e.printStackTrace();
      }
      Pattern regex = Pattern.compile("<access_token>(.*?)</access_token>", Pattern.DOTALL);
      Matcher matcher = regex.matcher(result);
      matcher.find();



      return matcher.group(1);
   }
   public static void listModifier(){
      try{
         File  newfile=new File("modified.txt");
         FileWriter fr=new FileWriter(newfile);

         final File f = new File("labelwithoutspace.txt");
         final List<String> lines = FileUtils.readLines(f);
         for(Iterator<String> iterator =lines.iterator(); iterator.hasNext();) {
            String string = iterator.next();
            //System.out.println(string);
            bearerTocken=getTocken(REST_URL);
            String responseCode=getRDFXML(string,bearerTocken);
            responseCode=responseCode.trim();

            if (responseCode.equals("No classes found for search String: "+string.trim())){

               System.out.println(true);

            }
            else{
               System.out.println(string);
               fr.write(string);
               fr.write("\n");


            }

         }
         fr.close();



      }
      catch (IOException e){
         e.printStackTrace();
      }





   }
   public static void spaceRemover(){
      File orginalFile=new File("labels.txt");
      File withoutSpace=new File("labelwithoutspace.txt");
      try{
         BufferedReader b = new BufferedReader(new FileReader(orginalFile));
         FileWriter fr=new FileWriter(withoutSpace);
         String readLine="";
         while((readLine=b.readLine())!=null){
            String newLine=readLine.replaceAll("\\s+","&");
            fr.write(newLine);
            fr.write("\n");
         }
         fr.close();
      }
      catch (IOException e){
         e.printStackTrace();
      }


   }
}

