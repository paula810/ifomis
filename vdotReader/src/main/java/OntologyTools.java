// This class will help to load ontology
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

import java.io.File;
import java.util.Arrays;

public class OntologyTools {
    OWLOntologyManager m= OWLManager.createOWLOntologyManager();
    OWLDataFactory df=OWLManager.getOWLDataFactory();

    public IRI convertStringToIRI(String ns) {
        return IRI.create(ns);
    }
    public OWLOntology loadOntology(String iri) throws OWLOntologyCreationException {
        File owlfile = new File(iri);
        return m.loadOntologyFromOntologyDocument(owlfile);
    }
    public OWLOntology createOntology(String iri) throws OWLOntologyCreationException {
        return createOntology(convertStringToIRI(iri));
    }
    public OWLOntology createOntology(IRI iri) throws OWLOntologyCreationException {
        return m.createOntology(iri);
    }
    public OWLClass createClass(String iri) {
        return createClass(convertStringToIRI(iri));
    }
    public OWLClass createClass(IRI iri) {
        return df.getOWLClass(iri);
    }
    public OWLAxiomChange createSubclass(OWLOntology o, OWLClass subclass, OWLClass superclass) {
        return new AddAxiom(o,df.getOWLSubClassOfAxiom(subclass, superclass));
    }

    public void applyChange(OWLAxiomChange ... axiom) {
        applyChanges(axiom);
    }

    private void applyChanges(OWLAxiomChange ... axioms) {
        m.applyChanges(Arrays.asList(axioms));
    }
    public OWLIndividual createIndividual(String iri) {
        return createIndividual(convertStringToIRI(iri));
    }
    private OWLIndividual createIndividual(IRI iri) {
        return df.getOWLNamedIndividual(iri);
    }

    public OWLAxiomChange associateIndividualWithClass(OWLOntology o,
                                                       OWLClass clazz,
                                                       OWLIndividual individual) {
        return new AddAxiom(o, df.getOWLClassAssertionAxiom(clazz, individual));
    }
    public OWLDataProperty createDataProperty(String iri) {
        return createDataProperty(convertStringToIRI(iri));
    }

    public OWLDataProperty createDataProperty(IRI iri) {
        return df.getOWLDataProperty(iri);
    }

    public OWLAxiomChange addDataToIndividual(OWLOntology o, OWLIndividual individual,
                                              OWLDataProperty property, String value) {
        OWLLiteral literal = df.getOWLLiteral(value, OWL2Datatype.XSD_STRING);
        return new AddAxiom(o, df.getOWLDataPropertyAssertionAxiom(property, individual, literal));
    }

    public OWLAxiomChange addDataToIndividual(OWLOntology o, OWLIndividual individual,
                                              OWLDataProperty property, boolean value) {
        OWLLiteral literal = df.getOWLLiteral(value);
        return new AddAxiom(o, df.getOWLDataPropertyAssertionAxiom(property, individual, literal));
    }

    public OWLAxiomChange addDataToIndividual(OWLOntology o, OWLIndividual individual,
                                              OWLDataProperty property, int value) {
        OWLLiteral literal = df.getOWLLiteral(value);
        return new AddAxiom(o, df.getOWLDataPropertyAssertionAxiom(property, individual, literal));
    }

}
