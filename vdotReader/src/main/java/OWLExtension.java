import com.mak.gitapi.GITTools;
import git.GITInitializer;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.semanticweb.owlapi.model.*;
import owlextension.ExtensionProparty;
import owlextension.OWLExtensionTools;
import owlextension.OntologyInitiator;
import owlextension.OntologyTools;

import java.io.*;

public class OWLExtension {
    public int getLatestVersionNumber() throws IOException {
        //Use database to get last record of version number
        FileInputStream in = new FileInputStream("version.rec");
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String strLine = null, tmp;

        while ((tmp = br.readLine()) != null)
        {
            strLine = tmp;
        }

        String lastLine = strLine;
        int version = Integer.parseInt(lastLine);
        System.out.println(lastLine);

        in.close();
        return  version ;
    }
    public void keepTrackofVersion(int versionNumber) throws IOException {
        //Save the version number in database
        File fout = new File("version.rec");
        BufferedWriter writer = new BufferedWriter(new FileWriter(fout));
        writer.write(Integer.toString(versionNumber));
        writer.close();
    }
    public String usingBufferedReader(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
        {

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null)
            {
                contentBuilder.append(sCurrentLine).append("\n");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    // versionNumber is the  newly the verson of newly extended ontology.
    // Source ontology is the ontology file we want to include inside VDOT.
    // subClassOf is class IRI of VDOT. Undeder which extension will occurs.
    // matchedClass is class IRI of the ontology that we want to add into VDOT.

    //Only run extend() method and then run uploadToGit(return of extend(), user info ) method

    public String extend(String sourceOntology, String subClassOf, String machedClass) throws OWLOntologyCreationException, IOException, OWLOntologyStorageException {
        int versionNumber = getLatestVersionNumber() ;
        OntologyInitiator initiator = new OntologyInitiator();
        OntologyTools mainontology_tools = new OntologyTools();
        OWLOntology mainontology = mainontology_tools.loadOntology(new File("VDOT_ALL.owl"));
        mainontology_tools.includeVersion(mainontology,Integer.toString(versionNumber+1));
        OWLOntology extraOntology = new OntologyTools().loadOntology(new File(sourceOntology));
        OWLDataFactory dataFactory_super = mainontology.getOWLOntologyManager().getOWLDataFactory();
        OWLClass superclass = dataFactory_super.getOWLClass(IRI.create(subClassOf));
        OWLDataFactory dataFactory_extra = extraOntology.getOWLOntologyManager().getOWLDataFactory();
        OWLClass subClassReference = dataFactory_extra.getOWLClass(IRI.create(machedClass));
        initiator.setMainOntology(mainontology);
        initiator.setExtraOntology(extraOntology);
        initiator.setSuperClass(superclass);
        initiator.setSubClassReference(subClassReference);
        OWLExtensionTools extensionTools = new OWLExtensionTools(initiator);
        ExtensionProparty.extensionDepth = 2;
        extensionTools.extend();
        int newVersiob = versionNumber + 1;
        String generatedFileName = "VDOT_ALL_"+ newVersiob+".owl";
        initiator.mainOntology.saveOntology(new FileOutputStream("gitrepo/"+generatedFileName));
        keepTrackofVersion(versionNumber + 1);
        return  generatedFileName;// return the file name of newly extended owl file.
    }
    public String uploadToGit(String genaratedFileName, String userInfo) throws IOException, GitAPIException {
        GITInitializer gitInit = new GITInitializer();
        gitInit.setCloneLink("https://github.com/IFOMIS/VDOT_EXT.git");
        gitInit.gitCommit(userInfo);
        return genaratedFileName;

    }
}
