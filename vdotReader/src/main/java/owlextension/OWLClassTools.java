package owlextension;

import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OWLClassTools {

    private static OWLOntology ontology;
    private static OWLClass _class;
    private static OWLOntologyManager manager;
    public static OWLReasoner reasoner;
    public String owlClassString;

    public OWLClassTools(OWLOntology ontology, OWLClass _class){
        this.ontology = ontology;
        this._class =_class;
        this.manager = ontology.getOWLOntologyManager();
        this.reasoner = new ElkReasonerFactory().createReasoner(ontology);
    }
    public OWLClassTools(String owlClassString){this.owlClassString = owlClassString;}

    public  static  Set<OWLClass> subClass(){
        return  reasoner.getSubClasses(_class,true).getFlattened();
    }

    public static Set<OWLClass> superClass(){
        return  reasoner.getSuperClasses(_class,true).getFlattened();
    }
    public  String getLabel(){
        Pattern regex = Pattern.compile("<rdfs:label>(.*?)</rdfs:label>", Pattern.DOTALL);
        Matcher matcher = regex.matcher(owlClassString);
        matcher.find();
        return  matcher.group(1);
    }
    public  Set<String> getExactSynonym(){
        return getRegexMatcher( "<oboInOwl:hasExactSynonym>(.*?)</oboInOwl:hasExactSynonym>");

    }

    private Set<String> getRegexMatcher(String s) {
        Pattern regex = Pattern.compile(s, Pattern.DOTALL);
        Matcher matcher = regex.matcher(owlClassString);
        Set<String> synonyms = new HashSet<String>();
        while(matcher.find()){
            //System.out.println(matcher.group(1));
            synonyms.add(matcher.group(1));
        }
        return synonyms;
    }

    public  Set<String> getRelatedSynonym(){
        return getRegexMatcher("<oboInOwl:hasRelatedSynonym>(.*?)</oboInOwl:hasRelatedSynonym>");

    }
    public  Set<String> getNarrowSynonym(){
        return getRegexMatcher("<oboInOwl:hasNarrowSynonym>(.*?)</oboInOwl:hasNarrowSynonym>");

    }
    public  Set<String> getSynonym(){
        return getRegexMatcher("<oboInOwl:hasSynonym>(.*?)</oboInOwl:hasSynonym>");

    }




}
