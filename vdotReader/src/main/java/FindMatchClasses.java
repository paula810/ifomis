import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import owlextension.OntologyInitiator;
import owlextension.OntologyTools;
import owlextension.SemanticClassMatcher;

import java.io.File;
import java.util.*;

// This class find the matches between REPO AND VDOT
public class FindMatchClasses {
    private Set<OWLClass> VDOTClasses = new HashSet<OWLClass>();
    private Set<OWLClass> RepositoryRankedClasses = new HashSet<OWLClass>();
    private OWLOntology VDOTOntology = null;
    private OWLOntology RepositoryOntology = null;
    //static final String DB_URL = "jdbc:mysql://localhost:3306/ifomis?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    OntologyInitiator initiator;
    public FindMatchClasses() throws OWLOntologyCreationException {
    }
    //static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //static final String USER = "root";
    //static final String PASS = "adminroot";

    public void setVDOTAddress(OWLOntology VDOTOntology, OWLOntology RepositoryOntology, Set<OWLClass> VDOTClasses,Set<OWLClass> RepositoryRankedClasses ) {
        this.VDOTOntology = VDOTOntology;
        this.RepositoryOntology = RepositoryOntology;
        this.VDOTClasses = VDOTClasses;
        this.RepositoryRankedClasses = RepositoryRankedClasses;
        initiator = new OntologyInitiator();
        initiator.setMainOntology(this.VDOTOntology);
        initiator.setExtraOntology(this.RepositoryOntology);
    }

//    Selecting all leaf classes
    public HashSet<OWLClass> getleafNode() throws OWLOntologyCreationException {

        HashSet<OWLClass> leafNode = new HashSet<OWLClass>();
        if (VDOTClasses == null){
            System.out.println("No VDOT class");
        }
        else {
            FindingClassesTools findVDOTClass = new FindingClassesTools(VDOTOntology);
            for (OWLClass VDOTClass : VDOTClasses) {
                Set<OWLClass> subclassses = findVDOTClass.getSubClass(VDOTClass);
                System.out.println("Sub class of vdot class" + subclassses);
                System.out.println("Sub class of vdot class" + subclassses.size());
              if( subclassses.size()>1){
                  continue;
              }
              else{
                  leafNode.add(VDOTClass);
              }

          }
        }
        return leafNode;
    }
//    This method will return all the matched class between REPO and VDOT
    public   Map<String, OWLClass> getMatchedClass() throws OWLOntologyCreationException {
       int maxMatch = 1 ;
       OWLClass MaxMatchVdotClasses = null;
       OWLClass MaxMatchREPOClasses = null;
       FindingClassesTools findVDOTClasses = new FindingClassesTools(VDOTOntology);
       FindingClassesTools findRepositoryClasses = new FindingClassesTools(RepositoryOntology);


       Set<OWLClass> vdotLeafNode = getleafNode();
       System.out.println("No OF VDOT LEAF NODES : "+vdotLeafNode.size());



       if (RepositoryRankedClasses !=null){
            for (OWLClass RepositoryClass : RepositoryRankedClasses){
                Set<OWLClass> repoSuperClass = findRepositoryClasses.getSuperClass(RepositoryClass);

                System.out.println("REPO SUPER CLASS"+ repoSuperClass);
                if (vdotLeafNode != null){
                    for (OWLClass VDOTClass : vdotLeafNode ){
                        Set<OWLClass> vdotSuperclass = findVDOTClasses.getSuperClass(VDOTClass);
                        //System.out.println("VDOT Super class " + vdotSuperclass);
                        //Set<OWLClass> intersection = new HashSet<OWLClass>(vdotSuperclass);
                        //System.out.println(intersection);
                        //intersection.retainAll(repoSuperClass);
                        int match = intersection(vdotSuperclass,repoSuperClass);
                        //System.out.println("Matched class" + intersection);
                        if(match > maxMatch){
                            maxMatch = match;
                            MaxMatchREPOClasses= RepositoryClass;
                            MaxMatchVdotClasses = VDOTClass;
                        }

                    }

                }






            }

        }
        //System.out.println("Matched class :"+ MaxMatchREPIRI);
        //System.out.println("Matched class :"+ MaxMatchVdotIRI);
        Map<String, OWLClass> matchedclass = new HashMap<String, OWLClass>();
        matchedclass.put("VDOTiri", MaxMatchVdotClasses);
        matchedclass.put("RANKEDiri", MaxMatchREPOClasses);


        return matchedclass;
    }

    public int intersection(Set<OWLClass> vdotSuperCLasses,Set<OWLClass> repoSuperCLasses){
        SemanticClassMatcher macher = new SemanticClassMatcher(initiator);
        int totalmatch = 0;
        Iterator<OWLClass> itr1 = vdotSuperCLasses.iterator();
        while(itr1.hasNext()){
            OWLClass class_owl = itr1.next();
            Iterator<OWLClass> itr2 =repoSuperCLasses.iterator();// There could be more efficient way, scope of improvement
            while(itr2.hasNext()){
               boolean b =  macher.findBestMatch(class_owl,itr2.next(),true);
                if(b){
                    totalmatch++;
                }
            }

        }
        return totalmatch;
    }

    public void extendVdot(){

    }
}
