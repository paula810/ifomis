import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Scanner;
import java.util.Set;

public class ExtensionTools {
//        public static void main(String [] argument) throws OWLException {
//            AddExtention(new File("New.owl"),new File("repo.owl"),"http://owl.api.tutorial#person", "http://purl.obolibrary.org/obo/GO_0035809");
//        }
    OWLOntology sourceOntology;// repo ontology
    OWLOntology destinationOntology; // vdot ontology
    OWLClass sourceClass; //repo class: All of the subclass of this class will be added to vdot ontlogy
    OWLClass destinationClass; // all of this repo class will go under this classs
        public ExtensionTools(OWLOntology sourceOntology, OWLOntology destinationOntology, OWLClass sourceClass,OWLClass destinationClass){
            this.sourceOntology = sourceOntology ;
            this.destinationOntology = destinationOntology;
            this.sourceClass = sourceClass;
            this.destinationClass = destinationClass;
        }

        public OWLOntology AddSingleExtension(OWLClass sourceSubclass) throws OWLOntologyCreationException {
            OWLOntologyManager manager = destinationOntology.getOWLOntologyManager();
            OWLDataFactory newdataFactory = manager.getOWLDataFactory();
            OWLAxiom axiom = newdataFactory.getOWLSubClassOfAxiom(sourceSubclass, destinationClass);
            AddAxiom addAxiom = new AddAxiom(destinationOntology, axiom);
            Set<OWLAnnotationAssertionAxiom> properties = sourceOntology.getAnnotationAssertionAxioms(sourceClass.getIRI());
            for (OWLAnnotationAssertionAxiom property : properties) {
                manager.applyChange(new AddAxiom(destinationOntology, property));
            }
            manager.applyChange(addAxiom);
            return destinationOntology;
        }

        public void AddExtention() throws OWLOntologyCreationException {
            OWLOntologyManager maneger = OWLManager.createOWLOntologyManager();
            OWLOntology ontology = sourceOntology;
            OWLDataFactory datafactory = ontology.getOWLOntologyManager().getOWLDataFactory();
            FindingClassesTools findClasses = new FindingClassesTools(sourceOntology);
            Set<OWLClass> subClasses = findClasses.getSubClass(sourceClass);
            maneger.removeOntology(ontology);
            OWLOntology newontology = destinationOntology;
            for(OWLClass o :subClasses){
                Set <OWLClass> subSubClass = findClasses.getSubClass(o);
                for(OWLAnnotationAssertionAxiom a :  ontology.getAnnotationAssertionAxioms(o.getIRI())){
                    if (a.getProperty().isLabel()){
                        if (a.getValue().toString().trim() != ""){
                            System.out.println("Do you want to add "+ " Following term," + " Then please write yes");
                            System.out.println(a.getValue());
                            System.out.println("It has following sub classes");
                            System.out.println(subSubClass);
                            Scanner scan = new Scanner(System.in);
                            if(scan.nextLine().equalsIgnoreCase("yes")){
                                newontology = AddSingleExtension(o);
                                break;
                            }
                        }

                    }
                }

            }

            try {
                newontology.saveOntology(new FileOutputStream("extention.txt"));
            } catch (OWLOntologyStorageException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

