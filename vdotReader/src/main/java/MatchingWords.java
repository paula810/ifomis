import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import info.debatty.java.stringsimilarity.Levenshtein;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by pallavi on 4/23/17.
 */
public class MatchingWords {
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSubClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSuperClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static BidiMap owlClassAndLabels=new DualHashBidiMap( );
    public static BidiMap originalVSLemmatized=new DualHashBidiMap();
    public static TreeMap<String,ArrayList<String>> vdotInvertedIndex=new TreeMap<String, ArrayList<String>>();
    public static BidiMap originalVSLemmatizedForRepo=new DualHashBidiMap();
    public static TreeMap<String,ArrayList<String>> repoTermAndLemmaList=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,List<String>> wordAndItsMatchForRepo=new TreeMap<String, List<String>>();
    public static TreeMap<String,ArrayList<String>> repoLabelAndVDOTMatchList=new TreeMap<String, ArrayList<String>>();
    public static  List<String> acceptedOntology=new ArrayList<String>();
    static List<String> stopwords = Arrays.asList("a", "able", "about",
            "across", "after", "all", "almost", "also", "am", "among", "an",
            "and", "any", "are", "as", "at", "be", "because", "been", "but",
            "by", "can", "cannot", "could", "dear", "did", "do", "does",
            "either", "else", "ever", "every", "for", "from", "get", "got",
            "had", "has", "have", "he", "her", "hers", "him", "his", "how",
            "however", "i", "if", "in", "into", "is", "it", "its", "just",
            "least", "let", "like", "likely", "may", "me", "might", "most",
            "must", "my", "neither", "no", "nor", "not", "of", "off", "often",
            "on", "only", "or", "other", "our", "own", "rather", "said", "say",
            "says", "she", "should", "since", "so", "some", "than", "that",
            "the", "their", "them", "then", "there", "these", "they", "this",
            "tis", "to", "too", "twas", "us", "wants", "was", "we", "were",
            "what", "when", "where", "which", "while", "who", "whom", "why",
            "will", "with", "would", "yet", "you", "your");



    public static void main(String args[]) throws OWLException, IOException, InterruptedException {



        //Load VDOT ontology
        IRI iri = IRI.create("http://www.ifomis.org/vdot/vdot_core.owl");
        OwlFileCreation(iri);
        //Modify the file

        fileModification(owlClassAndLabels,originalVSLemmatized);
        //Print the lemma
        //  printWordMatchTerms();
        //Create InvertedIndex
        createInvertedIndex();
        System.out.println("Enter the similarity percentage that you want");
        Scanner sc=new Scanner(System.in);
        double per=sc.nextDouble();

        //Get the API KEY
        GetLabelsFromRepo.API_KEY=GetLabelsFromRepo.getTocken(" http://xploit.ibmt.fraunhofer.de:8080/authorization-service/oauth/token?grant_type=password&username=paula810&password=Ihatethispassword123$");
        GetLabelsFromRepo.getLabelsFromFraunhoferRepo("mating");
        //Get labels from repo
        //lemmaChangeForRepositoryData(GetLabelsFromRepo.owlClassAndLabels);
        compareRepoNewAlgorithm(per);
        if(!wordAndItsMatchForRepo.isEmpty()) {
            System.out.println("The percentage of "+per+" match was found with the search term and here is the list");
            printTermsForRepo(wordAndItsMatchForRepo);
        }
        //If this is empty it means we couldnt get a match for the search word
        //now we create the algorithm for the labels with slight change of comparing the last term for per match
        else {
            //Comares the full term e.g if the lemma is "urine sample" then the full string will be compared with VDOT labels
            compareRepoAndVDOTLemma(per);
            //Compares only the last word e.g if the string is "urine sample" then only sample will be compared
            compareWordsRepo(per);
            //compares by reveresing, if the word is a 3 word string e.g "house of tree" then check for "tree house"
            //TODO: Insert function here
            System.out.println("The percentage of "+per+" match was NOT found with the search term, hence further search was made and found");

            getOriginalLabelForRepo();

            getOriginalLabelForVDOT();

        }

    }

    /**
     * The matched terms must be ranked per the 9 rules of ranks
     *
     */
    public static void rankings(){

    }

    /**
     * Implementing Rule 1- Contained in list of accepted ontologies
     */
    public static boolean rule1(String ontology){

        if(acceptedOntology.contains(ontology))
            return true;
        return false;
    }

    /**
     * List of all accepted ontologies
     */
    public static void addOntologiesToList(){
        acceptedOntology.add("GO");
        acceptedOntology.add("PATO");
        acceptedOntology.add("CHEBI");

    }


    /**
     * Check for full string match
     * @param per
     */
    public static void compareRepoAndVDOTLemma(double per){
        ArrayList<String> repolist=new ArrayList<String>();

        Iterator<Map.Entry<String, ArrayList<String>>> it=repoTermAndLemmaList.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String,ArrayList<String>> entry = it.next();
            String data=entry.getKey();
            repolist.addAll(entry.getValue());
        }
        MapIterator itt=originalVSLemmatized.mapIterator();
        while(itt.hasNext()){
            String key=itt.next().toString();
            String value=itt.getValue().toString();
            for(String s:repolist){
                double d=convertLevensteinToPercentage(value,s);
                if(d>=per)
                    System.out.println(value+" and "+s+" match to "+per+"%");
            }
            //System.out.println(key +" :::::::::: "+ value );
        }
    }
    /**
     * When the search term does not match, match with the labels that it generated from repo
     * @param per
     * @throws IOException
     */
    public static  void compareWordsRepo(double per) throws IOException {
        //Match only the last word for hit
        //Only when the complete word is not a hit
        Iterator<Map.Entry<String, ArrayList<String>>> it=repoTermAndLemmaList.entrySet().iterator();
        while (it.hasNext()) {
            ArrayList<String> allWordsMatch=new ArrayList<String>();
            Map.Entry<String,ArrayList<String>> entry = it.next();

            String data=entry.getKey();
            ArrayList<String> list=entry.getValue();
            for(String eachLine:list) {
                //String s=reverse(eachLine)
                //TODO: if(s!=null){
                //String[] split=s.splitArray(" ")
                String each=eachLine.substring(eachLine.lastIndexOf(" ")+1);
                if(stopwords.contains(each)) continue;
                Set<String> setOfKeys=vdotInvertedIndex.keySet();

                for(String s:setOfKeys){

                    double d=convertLevensteinToPercentage(each,s);
                    if(d>=per){

                        allWordsMatch.addAll(vdotInvertedIndex.get(s));
                        ArrayList<String> allWordsMatchRemoveDuplicate=new ArrayList<String>();
                        for(String word:allWordsMatch){
                            if(!allWordsMatchRemoveDuplicate.contains(word))
                                allWordsMatchRemoveDuplicate.add(word);
                        }
                        repoLabelAndVDOTMatchList.put(eachLine,allWordsMatchRemoveDuplicate);
                        if(wordAndItsMatchForRepo.containsKey(data)){


                            List<String> templist=wordAndItsMatchForRepo.get(data);

                            for(String vdotWord:allWordsMatchRemoveDuplicate) {
                                if(!templist.contains(vdotWord))
                                    templist.add(vdotWord);
                            }
                            wordAndItsMatchForRepo.put(data,templist);

                        }
                        else{
                            wordAndItsMatchForRepo.put(data,allWordsMatchRemoveDuplicate);
                        }

                    }
                }

            }


        }






    }

    /**
     * Given a lemmatized label, get the original label form
     */
    public static void getOriginalLabelForVDOT(){
        Iterator<Map.Entry<String, ArrayList<String>>> it=repoLabelAndVDOTMatchList.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String, ArrayList<String>> entry = it.next();
            String repoLabel=entry.getKey();
            ArrayList<String> list=entry.getValue();
            for(String s:list){
                if(originalVSLemmatized.containsValue(s)){
                    String originalValueOfVDOT=originalVSLemmatized.getKey(s).toString();

                    System.out.println(s+" ::::::::::::::::: "+originalValueOfVDOT);

                }
            }
        }
    }
    /**
     * From the maps of originalVsLemmatizedForRepo and repoLabelAndVDOTlist get the original label term(non lemmatized)
     */
    public static void getOriginalLabelForRepo(){
        Iterator<Map.Entry<String, ArrayList<String>>> it=repoLabelAndVDOTMatchList.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String, ArrayList<String>> entry = it.next();
            String repoLabel=entry.getKey();
            if(originalVSLemmatizedForRepo.containsValue(repoLabel))
            {
                String originalLabelOfRepo=originalVSLemmatizedForRepo.getKey(repoLabel).toString();
               // System.out.println("The original label for "+repoLabel+" is "+originalLabelOfRepo);
                if(GetLabelsFromRepo.allLabels.containsValue(originalLabelOfRepo)){
                    OWLAnnotationSubject correspondingSubject=(OWLAnnotationSubject)GetLabelsFromRepo.allLabels.getKey(originalLabelOfRepo);
                    if(GetLabelsFromRepo.allExactSynonyms.containsKey(correspondingSubject)){
                        System.out.println("The synonym for the label "+originalLabelOfRepo+" is "+GetLabelsFromRepo.allExactSynonyms.get(correspondingSubject));
                    }
                }

            }
        }
    }
    /**
     * Prints the repo terms that matches
     * @param mapset
     */
    public static void printTermsForRepo(TreeMap<String,List<String>> mapset){
        Iterator<Map.Entry<String, List<String>>> it=mapset.entrySet().iterator();
        System.out.println("The elements are(Combined) :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<String, List<String>> entry = it.next();

            System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Key:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: : "+entry.getKey());
            List<String> list=entry.getValue();
            for(String s:list)
                System.out.print("Value "+s+"\n");
        }
    }

    /**
     * Comparing the term with the repo and then compare last word
     * @param per
     */
    public static void compareRepoNewAlgorithm(double per){
        //As discussed, I am changing the similarity algorithm a bit

        //First up, compare if the search term  itself can give a per hit
        for(String searchWord:GetLabelsFromRepo.labels){
            ArrayList<String> allWordsMatch=new ArrayList<String>();
            Set<String> setOfKeys=vdotInvertedIndex.keySet();
            for(String s:setOfKeys) {

                double d = convertLevensteinToPercentage(searchWord, s);
                if(d>=per){

                    allWordsMatch.addAll(vdotInvertedIndex.get(s));
                    ArrayList<String> allWordsMatchRemoveDuplicate=new ArrayList<String>();
                    for(String word:allWordsMatch){
                        if(!allWordsMatchRemoveDuplicate.contains(word))
                            allWordsMatchRemoveDuplicate.add(word);
                    }
                    if(wordAndItsMatchForRepo.containsKey(searchWord)){


                        List<String> templist=wordAndItsMatchForRepo.get(searchWord);

                        for(String vdotWord:allWordsMatchRemoveDuplicate) {
                            if(!templist.contains(vdotWord))
                                templist.add(vdotWord);
                        }
                        wordAndItsMatchForRepo.put(searchWord,templist);
                    }
                    else{
                        wordAndItsMatchForRepo.put(searchWord,allWordsMatchRemoveDuplicate);
                    }
                }
            }
        }
    }
    /**
     * Calculates the levenstein distance between 2 words and converts to percentage
     * @param searchWord
     * @param owlWord - Label
     * @return percentage of similarity
     */
    public static double convertLevensteinToPercentage(String searchWord,String owlWord){
        // Refer to https://www.linkedin.com/pulse/percentage-string-match-levenshtein-distance-jacob-parr for algorithm
        Levenshtein l = new Levenshtein();//Function for levenshtein from info.debatty.java.stringsimilarity. ; apache commans can be used as well
        double len=Math.max(searchWord.length(),owlWord.length());
        double distance=len-l.distance(searchWord, owlWord);
        if(distance==0)
            return 0;
        double percentage=(distance/len)*100;
        return percentage;
    }
    /**
     * Convert the repository label list to its lemma
     * @param repo
     */
    public static void lemmaChangeForRepositoryData(TreeMap<String,ArrayList<String>> repo){
        Iterator<Map.Entry<String, ArrayList<String>>> it=repo.entrySet().iterator();


        while (it.hasNext()) {
            ArrayList<String> newWordList=new ArrayList<String>();
            Map.Entry<String,ArrayList<String>> entry = it.next();


            ArrayList<String> list=entry.getValue();
            for(String s:list){
                String lemmaString=lemmaOfWord(s);
                newWordList.add(lemmaString);
                originalVSLemmatizedForRepo.put(s,lemmaString);

            }
            repoTermAndLemmaList.put(entry.getKey(),newWordList);
            // System.out.println(s);
            //System.out.println("Value : "+entry.getValue());
        }

    }
    /**
     * Creates inverted Index from the vdot lemma
     * @throws IOException
     */
    public static void createInvertedIndex() throws IOException {

        MapIterator itVdot=originalVSLemmatized.mapIterator();
        while(itVdot.hasNext()){
            String original=itVdot.next().toString();
            String token=itVdot.getValue().toString();

            String splitArray[]=token.split(" ");
            for(String each:splitArray){
                if(stopwords.contains(each)) continue;

                if(vdotInvertedIndex.containsKey(each)){
                    ArrayList<String> list=vdotInvertedIndex.get(each);
                    if(!list.contains(token))
                        list.add(token);
                    vdotInvertedIndex.put(each,list);
                }
                else{
                    ArrayList<String> list=new ArrayList<String>();
                    list.add(token);
                    vdotInvertedIndex.put(each,list);
                }
            }


        }




    }
    /**
     * Creates the ontology from the IRI given
     * @param iri
     * @throws OWLOntologyCreationException
     * @throws IOException
     */
    public static void OwlFileCreation(IRI iri) throws OWLOntologyCreationException, IOException {
        //Import the ontology
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology pizzaOntology = manager.loadOntologyFromOntologyDocument(iri);
        Set<OWLOntology> allOntologies=manager.getImportsClosure(pizzaOntology);
        OWLReasonerFactory reasonerFactory = new ElkReasonerFactory();
        OWLReasoner reasoner = reasonerFactory.createReasoner(pizzaOntology);
        Set<OWLClass> clazz=pizzaOntology.getClassesInSignature(Imports.INCLUDED);
        Set<OWLAxiom> axiom= pizzaOntology.getAxioms(Imports.INCLUDED);
        for(OWLClass ow:clazz) {
            Set<OWLClass> subClasses = reasoner.getSubClasses(ow, true).getFlattened();
            owlClassAndSubClass.put(ow, subClasses);
            Set<OWLClass> superclasses = reasoner.getSuperClasses(ow, true).getFlattened();
            owlClassAndSuperClass.put(ow, superclasses);
            IRI cIRI = ow.getIRI();
            for (OWLOntology ontology : allOntologies) {
                for (OWLAnnotationAssertionAxiom a : ontology.getAnnotationAssertionAxioms(cIRI)) {


                    if (a.getProperty().isLabel()) {
                        if (a.getValue() instanceof OWLLiteral) {
                            OWLLiteral val = (OWLLiteral) a.getValue();

                            String v = val.getLiteral();

                            owlClassAndLabels.put(ow, v);

                        }
                    }
                }


            }
        }
    }

    /**
     * Takes the owlClassAndLabel map and retrives label
     * Makes the lemma of the labels and stores in originalvslemmatized
     * @param labelMap
     * @param lemmaMap
     */
    public static void fileModification(BidiMap labelMap,BidiMap lemmaMap) {
        MapIterator it = labelMap.mapIterator();
        while (it.hasNext()) {
            OWLClass cs = (OWLClass) it.next();

            String line = it.getValue().toString();

            String original = line;
            line = line.toLowerCase();

            line = line.replaceAll("[^\\p{L}\\p{Nd}]+", " ");
            String lemmaString = lemmaOfWord(line);
            lemmaMap.put(original, lemmaString);

        }
    }

    /**
     * Takes a word and converts to its lemma using stanford NLP
     * @param documentText
     * @return
     */
    public static String lemmaOfWord(String documentText){
        documentText.trim();
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        StanfordCoreNLP pipeline;
        pipeline = new StanfordCoreNLP(props);
        StringBuilder lemmas = new StringBuilder();
        // Create an empty Annotation just with the given textd
        Annotation document = new Annotation(documentText);
        // run all Annotators on this text
        pipeline.annotate(document);
        // Iterate over all of the sentences found
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
                lemmas.append(" ");
                lemmas.append(token.get(CoreAnnotations.LemmaAnnotation.class));
            }
        }
        return lemmas.toString().trim();
    }

    /**
     * Prints originalvsLemmatized map
     */
    public static void printWordMatchTerms(){
        MapIterator it=originalVSLemmatized.mapIterator();
        while(it.hasNext()){
            String key=it.next().toString();
            String value=it.getValue().toString();
            System.out.println(key +" :::::::::: "+ value );
        }
    }
}
