/**
 * Created by maktareq on 28.04.17.
 */


import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class tockenGenerator {
    static final String REST_URL = "http://xploit.ibmt.fraunhofer.de:8080/authorization-service/oauth/token?grant_type=password&username=s8mdkadi&password=CZ094gh401818898982!";
    static final String RDF_URL="http://xploit.ibmt.fraunhofer.de:8080/portal/rest/semanticRepository";
    static String bearerTocken="";
    static String getTocken(String urlToGet) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        String userCredentials = "xploit-trusted-client:Xpl0it!";
        String basicAuth = "Basic " + new String(DatatypeConverter.parseBase64Binary(userCredentials));
        try {
            url = new URL(urlToGet);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", basicAuth);
            conn.setRequestProperty("Accept", "application/xml");
            rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {

                result += line;
            }
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Pattern regex = Pattern.compile("<access_token>(.*?)</access_token>", Pattern.DOTALL);
        Matcher matcher = regex.matcher(result);
        matcher.find();
        System.out.println(matcher.group(1));
        return matcher.group(1);
    }


}