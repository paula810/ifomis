import org.semanticweb.owlapi.model.OWLClass;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;

public class Ranking{
    public static Connection conn;
    public static PreparedStatement statement;
    public static ResultSet resultSet;


    public static boolean Rule1_PredefinedList(OWLClass owlClass ){
        ArrayList<String> acceptableOntologies=new ArrayList<String>();
        acceptableOntologies.add("GO");
        acceptableOntologies.add("CHEBI");
        acceptableOntologies.add("PATO");





            String owlClassToString=owlClass.toString();
            String classId=owlClassToString.substring(owlClassToString.lastIndexOf('/')+1,owlClassToString.lastIndexOf('>'));
            for(String ontologies:acceptableOntologies){
               if( classId.contains(ontologies))
                   return  true;
            }

        return false;
    }

    public static boolean Rule2_ContainedInOBOFoundary(OWLClass owlClass){
           String owlClassToString=owlClass.toString();
            if(owlClassToString.contains("purl.obolibrary.org"))
                return true;

        return false;
    }

    public static boolean Rule3_DateCheck(OWLClass owlClass){
        try {
            //System.out.println(conn);
            String owlId=owlClass.toString();
            owlId=owlId.replaceAll("[<>]","");
            statement = conn.prepareStatement("SELECT * FROM owlTerms WHERE owlId = ?");
            statement.setString(1, owlId);
            resultSet = statement.executeQuery();


            while (resultSet.next()){
               // System.out.println("IN else");
                Date date=resultSet.getDate("created_date");
                //System.out.println(date);
                if(date!=null){
                    SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-DD");
                    java.util.Date compareDate=format.parse("2008-12-31");
                    Date newDate=new Date(compareDate.getTime());
                    if (date.after(newDate))
                        return true;
                }
            }
        }
        catch(Exception e){
            System.out.println("======================== Error: " + e + " =================================");
        }
        return false;
    }
    public static boolean Rule6_Author(OWLClass owlClass){
        try {
            String owlId=owlClass.toString();
            owlId=owlId.replaceAll("[<>]","");
            statement = conn.prepareStatement("SELECT * FROM owlTerms WHERE owlId = ?");
            statement.setString(1, owlId);
            resultSet = statement.executeQuery();

            while(resultSet.next()){
                String created_by=resultSet.getString("created_by");

                if(created_by!=null)
                    return true;
                }

        }
        catch(Exception e){
            System.out.println("======================== Error: " + e + " =================================");
        }
        return false;
    }
    public static boolean checkMoreThanOneSubClass(OWLClass targetClass){

        if(GetLabelsFromRepo.owlClassAndSubClass.containsKey(targetClass)){
            Set<OWLClass> valueSet=GetLabelsFromRepo.owlClassAndSubClass.get(targetClass);
            if(valueSet.size()>=1)
                return true;
        }
        return false;

    }
}