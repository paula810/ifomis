import java.util.Scanner;

/**
 * Created by pallavi on 3/8/17.
 */
public class SampleRead {
    public static void main(String [] args) {
        Scanner scanner = new Scanner(System.in);
        //System.out.println("Pallavi");
        int readValue = 0;
        while((readValue = scanner.nextInt()) != 5) {
            System.out.println("Send me something other than " + readValue);
        }
        System.out.println("Good job. Bye");
    }
}
