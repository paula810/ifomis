import com.mak.gitapi.GITTools;
import java.io.IOException;

public class UploadToGit {
    public void testCommitPush() throws IOException {
        GITTools gitTools = new GITTools();
        gitTools.setProtocol("https://");
        gitTools.setBaseURL("api.github.com/");
        gitTools.setRepo("Mak-Ta-Reque/test/");
        gitTools.setToken("c6dac2b121f912fe52a013ff555a5d8180a9d513");
        gitTools.setUsername("maktareq@gmail.com");
        gitTools.setRepo_branch("heads/master");
        //Store latest commit
        gitTools.setSha_latest_commit();
        System.out.println("Sha latest commit : " + gitTools.getSha_latest_commit());
        gitTools.setSha_base_tree();
        String basetree = gitTools.getSha_base_tree();
        System.out.println("Sha base tree : " + basetree);
        String post_body = "{\n" +
                "  \"base_tree\":"  + "\"" + basetree + "\"" +",\n" +
                "  \"tree\": [\n" +
                "    {\n" +
                "      \"path\": \"ExampleFile1111.txt\",\n" +
                "      \"mode\": \"100644\",\n" +
                "      \"type\": \"blob\",\n" +
                "      \"content\": \"This is NewFile1," + '"'+ " i am king of my kingdom.\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"path\": \"ExampleFile211.txt\",\n" +
                "      \"mode\": \"100644\",\n" +
                "      \"type\": \"blob\",\n" +
                "      \"content\": \"This is NfkkfhelhfehfewFile2.\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        System.out.println(post_body);
        gitTools.setPost_body(post_body);
        gitTools.setSha_new_tree();
        System.out.println("Sha new Tree : " + gitTools.getSha_new_tree());
        String commitmsg = "commit using git api";
        String commit_body = "{\n" +
                "  \"parents\": [\"" + gitTools.getSha_latest_commit()+ "\"],\n" +
                "  \"tree\": \""+ gitTools.getSha_new_tree() + "\",\n" +
                "  \"message\": \"" + commitmsg + "\"\n" +
                "}";
        gitTools.setCommit_body(commit_body);
        gitTools.setSha_new_commit();
        System.out.println("Sha new commit: " + gitTools.getSha_new_commit());
        String pushbodu ="{\n" +
                "  \"sha\": \"" + gitTools.getSha_new_commit() + "\"\n" +
                "}\n";
        gitTools.setPush_body(pushbodu);
        String ret= gitTools.commitPush();
        System.out.println(ret);
        //ret contains all the information about last post
    }
}

