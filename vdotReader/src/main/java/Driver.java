import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.obolibrary.oboformat.writer.OBOFormatWriter;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by pallavi on 7/4/17.
 */
public class Driver {
    public static TreeMap<OWLClass, Set<OWLClass>> owlClassAndSubClass = new TreeMap<OWLClass, Set<OWLClass>>();
    public static TreeMap<OWLClass, Set<OWLClass>> owlClassAndSuperClass = new TreeMap<OWLClass, Set<OWLClass>>();
    public static BidiMap owlClassAndLabels = new DualHashBidiMap();
    public static BidiMap originalVSLemmatized = new DualHashBidiMap();
    static final String DB_URL = "jdbc:mysql://localhost:3306/ifomis?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String USER = "root";
    static final String PASS = "admin";

    public static void main(String[] args) throws OWLException, IOException, InterruptedException {
        setUpDatabaseConnection();
        IRI iri = IRI.create("http://www.ifomis.org/vdot/vdot_core.owl");
        System.out.println("Enter the similarity percentage that you want");
        Scanner sc=new Scanner(System.in);
        double per=sc.nextDouble();
        Main.saveOntologyToFile(iri);
        Main.OwlFileCreation(iri);
        Main.fileModification(Main.owlClassAndLabels, Main.originalVSLemmatized);
        Main.fileModification(Main.owlClassAndSynonym,Main.originalSynonymVSLemmatizedSynonym);
        Main.printOwlClassAndLabel();
        Main.createInvertedIndex(Main.originalVSLemmatized,Main.vdotInvertedIndex);
        Main.createInvertedIndex(Main.originalSynonymVSLemmatizedSynonym,Main.vdotInvertedIndexForSynonym);
        Main.compareWords(per,"protein",Main.vdotInvertedIndex,Main.wordAndItsMatch);
        Main.compareWords(per,"protein",Main.vdotInvertedIndexForSynonym,Main.wordAndItsMatchForSynonyms);
        Main.printTerms(Main.wordAndItsMatch);

        Main.printTerms(Main.wordAndItsMatchForSynonyms);
        Set<OWLClass> vdotClasses= Main.getOwlID(Main.wordAndItsMatch);// MAtched classes from VDOT
        if(vdotClasses.size()==0){
            System.out.println("Sorry no class found. Contact Admin for more details!!");
            SendEmail.sendMail();
        }
        System.out.println("Number of matched vdot class "+ vdotClasses.size());

        //Set<OWLClass> leafNode = vdotExtension.getleafNode();



        //System.out.println("number of subclass "+ LEAFNODE.size());
        //setUpDatabaseConnection();
        GetLabelsFromRepo.API_KEY=GetLabelsFromRepo.getTocken("http://xploit.ibmt.fraunhofer.de:8080/authorization-service/oauth/token?grant_type=password&username=paula810&password=Ihatethispassword12345$$");;
        GetLabelsFromRepo.getLabelsFromFraunhoferRepo("protein");
        //Commenting Ranking Functionality
     //  Set<OWLClass> allRankedClassFromRepo= GetLabelsFromRepo.rankAll();
       // System.out.println("Matched class from repo: " +" (" + allRankedClassFromRepo.size()+" )" + allRankedClassFromRepo);
        //vdotExtension.getMatchedClass();

        ///GetLabelsFromRepo.printOwlClassAndLabel();
       // GetLabelsFromRepo.printExactSynonym();
//        Set<OWLClass> leafNode = vdotExtension.getleafNode();
      //This lines will load bt ontology and and their claases for finding matches.


        /*OWLOntologyManager manager_repository = OWLManager.createConcurrentOWLOntologyManager();
        OWLOntologyManager manager_VDOT = OWLManager.createConcurrentOWLOntologyManager();
        OWLOntology VDOTOntology = manager_VDOT.loadOntologyFromOntologyDocument(iri);
        OWLOntology RepositoryOntology = manager_repository.loadOntologyFromOntologyDocument(new File("repo.txt"));
        FindMatchClasses findMatches = new FindMatchClasses();
        findMatches.setVDOTAddress(VDOTOntology,RepositoryOntology,vdotClasses, allRankedClassFromRepo);
        Map<String, OWLClass> matchedClass = findMatches.getMatchedClass();
<<<<<<< .merge_file_a10120
        System.out.print(matchedClass);*/
        //System.out.print(matchedClass);
        //extention(matchedClass, RepositoryOntology, VDOTOntology);
    }
    public static void extention(Map<String,OWLClass> matchedpair, OWLOntology repositoryOntology, OWLOntology VDOTontology){
        OWLClass vdotClass = matchedpair.get("VDOTiri");
        OWLClass repoClass = matchedpair.get("RANKEDiri");
        ExtensionTools extTools = new ExtensionTools(repositoryOntology,VDOTontology,repoClass,vdotClass);
    }
    public static void setUpDatabaseConnection(){
        Connection conn = null;
        try {
            System.out.println("Connecting...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            GetLabelsFromRepo.conn=conn;
            Ranking.conn=conn;
        }
        catch(SQLException e){
            System.out.println("========== Error: " + e + " ==========");
            System.out.println("Couldn't establish connection to the database.");
        }
    }
}

