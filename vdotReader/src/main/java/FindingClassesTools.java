import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.io.File;
import java.util.Set;

// This is a universal script for finding all super class and sub class given the ontology  and a classs iri
public class FindingClassesTools {
    Set<OWLClass> superClasses = null;
    Set<OWLClass> subClasses = null;
    OWLOntologyManager manager;
    OWLOntology ontology;
    OWLReasoner reasoner;


    public FindingClassesTools(OWLOntology ontology) throws OWLOntologyCreationException {// constructor will store the source file of ontology
        this.ontology = ontology;
        manager = ontology.getOWLOntologyManager();
        reasoner = new ElkReasonerFactory().createReasoner(ontology);
    }


    public Set<OWLClass> getSuperClass(OWLClass givenClass) throws OWLOntologyCreationException {//Return the super class of a given iri
        superClasses = reasoner.getSuperClasses(givenClass, false).getFlattened();
        return superClasses;
    }


    public Set<OWLClass> getSubClass(OWLClass givenClass) throws OWLOntologyCreationException {//Return the sub class of a given iri
        subClasses = reasoner.getSubClasses(givenClass, false).getFlattened();
        return subClasses;
    }


    public  int getSubClassCount(){//return number of sub class
        return subClasses.size();
    }


    public int getSuperClassCount(){//return number of superclass
        return superClasses.size();
    }

}
