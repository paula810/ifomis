/**
 * Created by pallavi on 3/31/17.
 */
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.io.FileUtils;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.apache.commons.codec.binary.Base64;
import org.semanticweb.owlapi.search.EntitySearcher;

public class GetLabelsFromRepo {
    public static Connection conn;
    public static PreparedStatement statement;
    public static ResultSet resultSet;
    static final String REST_URL = "http://xploit.ibmt.fraunhofer.de:8080/portal/rest/semanticRepository";
    static  String API_KEY ="";
    static final ObjectMapper mapper = new ObjectMapper();
    public static final String SYNONYM_PROPERTY_KEY = "synonym_properties";
    public static final String SYNONYM_PROPERTY_URI = "http://www.geneontology.org/formats/oboInOwl#hasExactSynonym";
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSubClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSuperClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,String> owlClassAndLabels=new TreeMap<OWLClass, String>();
    public static Set<OWLClass> owlIds=new HashSet<OWLClass>();
    public static DualHashBidiMap allLabels = new DualHashBidiMap();
    public static Map<OWLAnnotationSubject, String> allExactSynonyms = new HashMap<OWLAnnotationSubject,String>();
    public static Map<OWLAnnotationSubject, String> allCreationDates = new HashMap<OWLAnnotationSubject,String>();
    public static Map<OWLAnnotationSubject, String> allCreatedBy = new HashMap<OWLAnnotationSubject,String>();
        public static ArrayList<String> labels = new ArrayList<String>();
    private static int INDENT = 4;
    public static OWLOntology pizzaOntology;

public static void main(String[] args) throws OWLException, InterruptedException, IOException {
    getLabelsFromFraunhoferRepo("mating");

}


    public static void getLabelsFromFraunhoferRepo(String retrievedLabel) throws IOException, OWLException, InterruptedException {
        //System.out.println(conn);

             ArrayList<String> allOwlLabels =new ArrayList<String>();
            String ontologies_string="";
            try {
                 ontologies_string = get(REST_URL + "/query?search="+retrievedLabel);
            }
            catch (Exception e){
                System.out.println("Term not found. Contact admin for more details.");
                SendEmail.sendMail();
                //break;
            }

            File f = new File("repo.txt");
            FileUtils.writeStringToFile(f, ontologies_string);
            TimeUnit.SECONDS.sleep(5);

            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            pizzaOntology = manager.loadOntologyFromOntologyDocument(f);
            Set<OWLOntology> allOntologies=manager.getImportsClosure(pizzaOntology);
            OWLReasonerFactory reasonerFactory = new ElkReasonerFactory();
            OWLReasoner reasoner = reasonerFactory.createReasoner(pizzaOntology);
            Set<OWLClass> clazz=pizzaOntology.getClassesInSignature(Imports.INCLUDED);
            Set<OWLAxiom> axiom= pizzaOntology.getAxioms(Imports.INCLUDED);
            for(OWLClass ow:clazz) {
                Set<OWLClass> subClasses = reasoner.getSubClasses(ow, true).getFlattened();
                owlClassAndSubClass.put(ow, subClasses);
                Set<OWLClass> superclasses = reasoner.getSuperClasses(ow, true).getFlattened();
                owlClassAndSuperClass.put(ow, superclasses);

                printHierarchy(reasoner,ow,1);
                    IRI cIRI = ow.getIRI();

                for (OWLOntology ontology : allOntologies) {
                    for (OWLAnnotationAssertionAxiom a : ontology.getAnnotationAssertionAxioms(cIRI)) {


                        if (a.getProperty().isLabel()) {
                            if (a.getValue() instanceof OWLLiteral) {
                                OWLLiteral val = (OWLLiteral) a.getValue();

                                String v = val.getLiteral();
                                String owlId=ow.toString();

                                owlId=owlId.replaceAll("[<>]","");
                                owlIds.add(ow);

                               // insertLabel(v,owlId);//Commented by Me

                               // insertLabel(v,owlId);




                            }
                        }

                    }
                    for (OWLAnnotationAssertionAxiom ax : ontology.getAxioms(AxiomType.ANNOTATION_ASSERTION)){
                        OWLAnnotationSubject subject = ax.getSubject();
                        String synonym="";
                        String creation_date="";
                        String created_by="";
                        if (ax.getProperty().toString().contains("hasExactSynonym")) {
                            synonym=ax.getValue().toString();

                        }
                        if (ax.getProperty().toString().contains("creation_date")) {
                            creation_date=ax.getValue().toString();

                            if(!creation_date.equals("")) {
                                creation_date=creation_date.replace("\"","");
                                creation_date=creation_date.substring(0,creation_date.lastIndexOf("Z"));


                                //updateCreatedDate(subject.toString(), creation_date);//Commented by me

                               // updateCreatedDate(subject.toString(), creation_date);

                            }

                        }
                        if (ax.getProperty().toString().contains("created_by")) {
                            created_by=ax.getValue().toString();
                            created_by=created_by.replaceAll("[^a-zA-Z0-9]", "");

                            //updateCreatedBy(subject.toString(),created_by);// Commented by me

                               // updateCreatedBy(subject.toString(),created_by);

                        }
                    }



                }
            }
           // f.delete();


    }
    public static Set<OWLClass> rankAll(){
        Set<OWLClass> rankedClass=new HashSet<OWLClass>(); //Final Result Ranked class of Repo
      for(OWLClass ow:owlIds){
        //  System.out.println("Current Owl Id is "+ow);
          boolean rule1=Ranking.Rule1_PredefinedList(ow);
          boolean rule2=Ranking.Rule2_ContainedInOBOFoundary(ow);
          boolean rule3=Ranking.Rule3_DateCheck(ow);
          boolean rule6=Ranking.Rule6_Author(ow);
          boolean rule9=Ranking.checkMoreThanOneSubClass(ow);
          System.out.println("For owlId : "+ow+" Rule 1: "+rule1+" Rule 2 : "+rule2+" Rule 3 : "+rule3+" Rule 6 : "+rule6+" Rule 9 : "+rule9);
          if(rule1 && rule2 && rule3 && rule6 && rule9){
              rankedClass.add(ow);
          }


      }

        System.out.println("The ranked classes are: ");
      for(OWLClass ow:rankedClass){
          System.out.println(ow);
      }
        return rankedClass;
    }

/*    public static HashMap<OWLClass,Set<OWLClass>> getAllSuperClass() throws OWLOntologyCreationException {
        for(OWLClass mainClass:owlClassAndSuperClass.keySet()){
            Iterator it=owlClassAndSuperClass.get(mainClass).iterator();
            while (it.hasNext()){

            }

        }

    }*/

    private static void printHierarchy(OWLReasoner reasoner, OWLClass clazz, int level)
            throws OWLException {
        /*
         * Only print satisfiable classes -- otherwise we end up with bottom
         * everywhere
         */
        if (reasoner.isSatisfiable(clazz)) {
            for (int i = 0; i < level * INDENT; i++) {
                System.out.print(" ");
            }
            System.out.println(clazz);
            /* Find the children and recurse */
            for (OWLClass child : reasoner.getSubClasses(clazz, true).getFlattened()) {
                if (!child.equals(clazz)) {
                    printHierarchy(reasoner, child, level + 1);
                }
            }
        }
    }
    private static String labelFor(OWLClass clazz) {
        for (OWLAnnotationAssertionAxiom a : pizzaOntology.getAnnotationAssertionAxioms(clazz.getIRI())) {
            if (a.getProperty().isLabel()) {
                if (a.getValue() instanceof OWLLiteral) {
                    OWLLiteral val = (OWLLiteral) a.getValue();
                    String v = val.getLiteral();
                    return v;
                }
            }
        }
        return null;
    }
    public static boolean updateCreatedDate(String owlId,String createdDate){
        try{
            statement = conn.prepareStatement("UPDATE owlTerms SET  created_date = ? WHERE owlId = ?");
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            java.util.Date dateToAdd=simpleDateFormat.parse(createdDate);
            java.sql.Date sqlStartDate = new java.sql.Date(dateToAdd.getTime());
            statement.setDate(1, sqlStartDate);

            statement.setString(2, owlId);
            statement.executeUpdate();

            statement = conn.prepareStatement("SELECT * FROM owlTerms WHERE owlId = ?");
            statement.setString(1, owlId);
            resultSet = statement.executeQuery();
            if(resultSet.next())
                return true;
        } catch (Exception e){
            System.out.println("========== Error: " + e + " ==========");
        }
        return false;

    }
    public static boolean updateCreatedBy(String owlId,String createdBy){
        try{
            statement = conn.prepareStatement("UPDATE owlTerms SET  created_by = ? WHERE owlId = ?");

            statement.setString(1, createdBy);

            statement.setString(2, owlId);
            statement.executeUpdate();

            statement = conn.prepareStatement("SELECT * FROM owlTerms WHERE owlId = ?");
            statement.setString(1, owlId);
            resultSet = statement.executeQuery();
            if(resultSet.next())
                return true;
        } catch (Exception e){
            System.out.println("========== Error: " + e + " ==========");
        }
        return false;

    }
    public static boolean insertLabel(String label,String owlId){
        try{
            statement = conn.prepareStatement("SELECT * FROM owlTerms WHERE owlId = ?");
            statement.setString(1, owlId);
            resultSet = statement.executeQuery();
            if(resultSet.next())
                return false;
            else{
                statement = conn.prepareStatement("INSERT INTO owlTerms (owlId,label) VALUES(?, ?)");
                statement.setString(1, owlId);
                statement.setString(2, label);

                statement.executeUpdate();

                statement = conn.prepareStatement("SELECT * FROM owlTerms WHERE owlId = ?");
                statement.setString(1, owlId);
                resultSet = statement.executeQuery();

                if(resultSet.next()){
                    return true;
                  //  System.out.println("Successful creation of table");
                }
                else
                    System.out.println("===================== Error ==============================");

            }

        } catch(Exception e){
            System.out.println("======================== Error: " + e + " =================================");
        }
        return true;
    }
    private static String getResourceString(ResourceBundle rb, String key, String defaultValue) {
        String ret;
        try {
            ret = rb.getString(key);
            System.out.println(ret);
        } catch (MissingResourceException mre) {
            ret = defaultValue == null ? "" : defaultValue;
        }
        return ret;
    }

    public static void printOwlClassAndLabel(){
        Iterator<Map.Entry<OWLClass, String>> it=owlClassAndLabels.entrySet().iterator();
        System.out.println("The elements are(Owl Class and Label) ::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<OWLClass,String> entry = it.next();

            System.out.println("Key : "+entry.getKey());
            String list=entry.getValue();
            System.out.println("Value : "+list);

        }
    }
    public static void printExactSynonym(){
        Iterator<Map.Entry<OWLAnnotationSubject, String>> it=allCreatedBy.entrySet().iterator();
        System.out.println("The elements are(Created By) ::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<OWLAnnotationSubject,String> entry = it.next();

            System.out.println("Key : "+entry.getKey());
            String list=entry.getValue();
            System.out.println("Value : "+list);

        }
    }

    private static String get(String urlToGet) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        API_KEY = tockenGenerator.getTocken(tockenGenerator.REST_URL);
        try {
            url = new URL(urlToGet);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "bearer" + API_KEY);
            conn.setRequestProperty("Accept", "application/xml");
            rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public static String getTocken(String urlToGet) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        String userCredentials = "xploit-trusted-client:Xpl0it!";
        String basicAuth = "Basic " + new String(new Base64().encode(userCredentials.getBytes()));
        try {
            url = new URL(urlToGet);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", basicAuth);
            conn.setRequestProperty("Accept", "application/xml");
            rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {

                result += line;
            }
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Pattern regex = Pattern.compile("<access_token>(.*?)</access_token>", Pattern.DOTALL);
        Matcher matcher = regex.matcher(result);
        matcher.find();



        return matcher.group(1);
    }



}
