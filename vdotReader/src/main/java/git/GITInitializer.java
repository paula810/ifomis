package git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;
import java.io.IOException;

public class GITInitializer {
    private String cloneLink = "https://github.com/IFOMIS/VDOT_EXT.git";
    private String branch = "";
    private String email = "ifomis2017@gmail.com";
    private String username = "VDOTCurator";
    private String password = "saarland12345";

    public void setCloneLink(String cloneLink) {
        this.cloneLink = cloneLink;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
    public void gitClone() throws IOException, GitAPIException {
        try (Git result = Git.cloneRepository()
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username,password))
                .setURI(cloneLink)
                .setDirectory(new File("gitrepo"))
                .call()) {
            // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!
            System.out.println("Having repository: " + result.getRepository().getDirectory());
            result.close();

        }



    }
    public void gitCommit(String userInfo) throws IOException, GitAPIException {
        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        Repository repository = repositoryBuilder.setGitDir(new File("gitrepo/.git"))
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .setMustExist(true)
                .build();
        try (Git git = new Git(repository)) {
            // Stage all files in the repo including new files
            git.add().addFilepattern(".").call();

            // and then commit the changes.

            // Stage all changed files, omitting new files, and commit with one command
            git.commit()
                    .setAll(true)
                    .setMessage(userInfo)
                    .call();


            System.out.println("Committed all changes to repository at " + repository.getDirectory());

            git.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password)).call();
            git.close();

        }

    }
}
