import com.fasterxml.jackson.databind.ObjectMapper;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by pallavi on 3/31/17.
 */
public class GetLabelForSearch {
    static final String REST_URL = "http://data.bioontology.org";
    static final String API_KEY = "15290308-127c-42a2-9850-2af157701127";
    static final ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] args) {
        ArrayList<String> labels = new ArrayList<String>();

        // Get all ontologies from the REST service and parse the JSON
        String ontologies_string = get(REST_URL + "/search?q=mating");
        System.out.println(ontologies_string);


    }



    private static String get(String urlToGet) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        try {
            url = new URL(urlToGet);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "apikey token=" + API_KEY);
            conn.setRequestProperty("Accept", "application/xml");
            rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
