import RepoAccessTools.RetriveData;
import owlextension.OWLClassTools;

public class LabelExample {
    public static void main(String[] args){
        RetriveData retriveRepoData = new RetriveData();
        String query = "http://purl.obolibrary.org/obo/GO_1903322";
        String queryType = "queryClass";
        String repoResource  = retriveRepoData.getRepositoryResource(query,queryType);
        //System.out.println(repoResource);
        OWLClassTools classTools = new OWLClassTools(repoResource);
        System.out.println(classTools.getLabel());
        // Different types of synonyms returned by different method
        System.out.println(classTools.getExactSynonym());
        System.out.println(classTools.getRelatedSynonym());
        System.out.println(classTools.getSynonym());
        System.out.println(classTools.getNarrowSynonym());





    }
}
