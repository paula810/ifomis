import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.io.FileUtils;
import org.openrdf.model.vocabulary.OWL;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.apache.commons.io.LineIterator;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.apache.commons.collections.BidiMap;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;


import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.*;
import info.debatty.java.stringsimilarity.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.magicwerk.brownies.collections.BigList;

import org.semanticweb.elk.owlapi.ElkReasoner;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

public class Main {



    public static TreeMap<String,ArrayList<String>> vdotInvertedIndex=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,ArrayList<String>> vdotInvertedIndexForSynonym=new TreeMap<String, ArrayList<String>>();


    public static TreeMap<String,ArrayList<String>> wordAndItsMatch=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,ArrayList<String>> wordAndItsMatchForSynonyms=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,List<String>> wordAndItsMatchForRepo=new TreeMap<String, List<String>>();
    public static TreeMap<String,ArrayList<String>> wordAndItsMatchOfNewSet=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,ArrayList<String>> wordAndItsMatchPato=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,ArrayList<String>> wordAndItsMatchChebi=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,ArrayList<String>> wordAndItsMatchGene=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSubClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSuperClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSubClassOfNewMatch=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> owlClassAndSuperClassOfNewMatch=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchedowlClassAndSuperClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchowlClassAndSubClass=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchedowlClassAndSuperClassPATO=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchowlClassAndSubClassPATO=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchedowlClassAndSuperClassGENE=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchowlClassAndSubClassGENE=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchedowlClassAndSuperClassCHEBI=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<OWLClass,Set<OWLClass>> finalMatchowlClassAndSubClassCHEBI=new TreeMap<OWLClass,Set<OWLClass>>();
    public static TreeMap<String,ArrayList<String>> repoTermAndLemmaList=new TreeMap<String, ArrayList<String>>();
    public static TreeMap<String,ArrayList<String>> repoLabelAndVDOTMatchList=new TreeMap<String, ArrayList<String>>();
    public static BidiMap owlClassAndLabels=new DualHashBidiMap( );
    public static BidiMap owlClassAndSynonym=new DualHashBidiMap( );
    public static BidiMap owlClassAndLabelsOfNewMatch=new DualHashBidiMap( );
    public static BidiMap originalVSLemmatized=new DualHashBidiMap( );
    public static BidiMap originalSynonymVSLemmatizedSynonym=new DualHashBidiMap( );
    public static BidiMap originalVSLemmatizedForRepo=new DualHashBidiMap( );
    public static BidiMap originalVSLemmatizedOfNewSet=new DualHashBidiMap( );
    //public static BigList<String> biglist=new BigList<String>();
    static List<String> stopwords = Arrays.asList("a", "able", "about",
            "across", "after", "all", "almost", "also", "am", "among", "an",
            "and", "any", "are", "as", "at", "be", "because", "been", "but",
            "by", "can", "cannot", "could", "dear", "did", "do", "does",
            "either", "else", "ever", "every", "for", "from", "get", "got",
            "had", "has", "have", "he", "her", "hers", "him", "his", "how",
            "however", "i", "if", "in", "into", "is", "it", "its", "just",
            "least", "let", "like", "likely", "may", "me", "might", "most",
            "must", "my", "neither", "no", "nor", "not", "of", "off", "often",
            "on", "only", "or", "other", "our", "own", "rather", "said", "say",
            "says", "she", "should", "since", "so", "some", "than", "that",
            "the", "their", "them", "then", "there", "these", "they", "this",
            "tis", "to", "too", "twas", "us", "wants", "was", "we", "were",
            "what", "when", "where", "which", "while", "who", "whom", "why",
            "will", "with", "would", "yet", "you", "your");
    public static void main(String[] args) throws OWLException, IOException, InterruptedException {

        System.setProperty("log4j.configuration","file:///media/mak/52E8EFC5E8EFA587/HIWI/IFOMIS/ifomis/vdotReader/src/main/java/log4j.properties");

       //Take the vdot_core IRI and get all labels
        IRI iri = IRI.create("http://www.ifomis.org/vdot/vdot_core.owl");
        OwlFileCreation(iri);
        //Prints the map of class and labels
        //printOwlTerms();
        //Remove all " " from the file and @de and @en and special characters for all files and take the lemma and save
        fileModification(owlClassAndLabels,originalVSLemmatized);
        //Print the originalVSLemmatized
        printWordMatchTerms();
        //patofileModification();
        //chebifileModification();combinedfileModification();genefileModification();

        //Create InvertedIndex
        //createInvertedIndex();

        //printVdotTerms();
        System.out.println("Enter the similarity percentage that you want");
        Scanner sc=new Scanner(System.in);
        double per=sc.nextDouble();
        //Compare Terms for combined
        //compareWords(per);
        //Check contents for combined
       //printTerms(wordAndItsMatch);
        //Compare terms for Pato
       //patoSimilarity(per);
        //Check contents for combined
        //printTerms(wordAndItsMatchPato);
        //Compare terms for Gene
        //geneSimilarity(per);
        //Check contents for Gene
        //printTerms(wordAndItsMatchGene);
        //Compare terms for Chebi
       /* chebiSimilarity(per);
        //Check contents for Chebi
        printTerms(wordAndItsMatchChebi);
        //Get Sub and Super Class of combined
        if(!wordAndItsMatch.isEmpty()) {
            //System.out.println("The sub and superclass of all terms with VDOT are :::::::::::::::::::::::::::::::::");
            getClassValue(wordAndItsMatch,finalMatchowlClassAndSubClass,finalMatchedowlClassAndSuperClass);
        }
        if(!wordAndItsMatchChebi.isEmpty()) {
            //Get Sub and Super Class of chebi
            //System.out.println("The sub and superclass of chebi terms with VDOT are :::::::::::::::::::::::::::::::::");
            getClassValue(wordAndItsMatchChebi,finalMatchowlClassAndSubClassCHEBI,finalMatchedowlClassAndSuperClassCHEBI);
        }
        if(!wordAndItsMatchPato.isEmpty()) {
            //Get Sub and Super Class of pato
          //  System.out.println("The sub and superclass of PATO terms with VDOT are :::::::::::::::::::::::::::::::::");
            getClassValue(wordAndItsMatchPato,finalMatchowlClassAndSubClassPATO,finalMatchedowlClassAndSuperClassPATO);
        }
        if(!wordAndItsMatchGene.isEmpty()) {
            //Get Sub and Super Class of comparison with Gene
           // System.out.println("The sub and superclass of Gene terms with VDOT are :::::::::::::::::::::::::::::::::");
            getClassValue(wordAndItsMatchGene,finalMatchowlClassAndSubClassGENE,finalMatchedowlClassAndSuperClassGENE);
        }
        System.out.println("The super class terms of CHEBi with VDOT are");
        printFinalChebiTerms(finalMatchedowlClassAndSuperClassCHEBI);
        System.out.println("The sub class terms of CHEBI with VDOT are");

        System.out.println("The super class terms of PATO with VDOT are");
        printFinalChebiTerms(finalMatchedowlClassAndSuperClassPATO);
        System.out.println("The sub class terms of PATO with VDOT are");
        printFinalChebiTerms(finalMatchowlClassAndSubClassPATO);

        System.out.println("The super class terms of Gene with VDOT are");
        printFinalChebiTerms(finalMatchedowlClassAndSuperClassGENE);
        System.out.println("The sub class terms of Gene with VDOT are");
        printFinalChebiTerms(finalMatchowlClassAndSubClassGENE);

        System.out.println("The super class terms of All with VDOT are");
        printFinalChebiTerms(finalMatchedowlClassAndSuperClass);
        System.out.println("The sub class terms of All with VDOT are");
        printFinalChebiTerms(finalMatchowlClassAndSubClass);

        //For each of the super and sub class the same comparision would be made

        executeProcessAgain(finalMatchowlClassAndSubClass);
        System.out.println("Done");
        fileModification(owlClassAndLabelsOfNewMatch,originalVSLemmatizedOfNewSet);
        compareWords(per,originalVSLemmatizedOfNewSet);
        printTerms(wordAndItsMatchOfNewSet);
        printOwlTerms();
        if(!wordAndItsMatchOfNewSet.isEmpty()) {
            //System.out.println("The sub and superclass of all terms with VDOT are :::::::::::::::::::::::::::::::::");
            finalMatchedowlClassAndSuperClass.clear();
            finalMatchowlClassAndSubClass.clear();
            getValuesForBoth(wordAndItsMatchOfNewSet,finalMatchowlClassAndSubClass,finalMatchedowlClassAndSuperClass);
        }*/
        //Get the labels from Fraunhfer repo
        GetLabelsFromRepo.API_KEY=tockenGenerator.getTocken(tockenGenerator.REST_URL);
        //GetLabels.getLabelsFromBioPortal();
       GetLabelsFromRepo.getLabelsFromFraunhoferRepo("mating");
       //GetLabelsFromRepo.printOwlClassAndLabel();
       // lemmaChangeForRepositoryData(GetLabelsFromRepo.owlClassAndLabels);
       // printTerms(repoTermAndLemmaList);

        compareRepoNewAlgorithm(per);
        if(!wordAndItsMatchForRepo.isEmpty()) {
            System.out.println("The percentage of "+per+" match was found with the search term and here is the list");
            printTermsForRepo(wordAndItsMatchForRepo);
        }
        //If this is empty it means we couldnt get a match for the search word
        //now we create the algorithm for the labels with slight change of comparing the last term for per match
        else {
            compareWordsRepo(per);
            System.out.println("The percentage of "+per+" match was NOT found with the search term, hence further search was made and found");
            //printTermsForRepo(wordAndItsMatchForRepo);
            //printTerms(repoLabelAndVDOTMatchList);
            getOriginalLabelForRepo();
            getOriginalLabelForVDOT();

        }
    }

    public static void getOriginalLabelForVDOT(){
        Iterator<Map.Entry<String, ArrayList<String>>> it=repoLabelAndVDOTMatchList.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String, ArrayList<String>> entry = it.next();
            String repoLabel=entry.getKey();
            ArrayList<String> list=entry.getValue();
            for(String s:list){
                if(originalVSLemmatized.containsValue(s)){
                    String originalValueOfVDOT=originalVSLemmatized.getKey(s).toString();
                    System.out.println(s+" ::::::::::::::::: "+originalValueOfVDOT);

                }
            }
        }
    }
    /**
     * From the maps of originalVsLemmatizedForRepo and repoLabelAndVDOTlist get the original label term(non lemmatized)
     */
    public static void getOriginalLabelForRepo(){
        Iterator<Map.Entry<String, ArrayList<String>>> it=repoLabelAndVDOTMatchList.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String, ArrayList<String>> entry = it.next();
            String repoLabel=entry.getKey();
            if(originalVSLemmatizedForRepo.containsValue(repoLabel))
            {
                String originalLabelOfRepo=originalVSLemmatizedForRepo.getKey(repoLabel).toString();
                System.out.println("The original label for "+repoLabel+" is "+originalLabelOfRepo);

            }
        }
    }
    /**
     * Gets the sub and superclass from the TreeMap
     * @param map
     */
    public static void getValuesForBoth(TreeMap<String,ArrayList<String>> map,TreeMap<OWLClass,Set<OWLClass>> subclass,TreeMap<OWLClass,Set<OWLClass>> superclass){
        Iterator<Map.Entry<String, ArrayList<String>>> it=map.entrySet().iterator();
        // System.out.println("The sub and super classes are ::::::::::::::::::::::");

        while (it.hasNext()) {
            Map.Entry<String, ArrayList<String>> entry = it.next();

             String lemma=entry.getKey();//Got the lemma of the new set
             if(originalVSLemmatizedOfNewSet.containsValue(lemma)){
                 //if this is present then take the original value
                 String originallabel=originalVSLemmatizedOfNewSet.getKey(lemma).toString();
                 if(owlClassAndLabelsOfNewMatch.containsValue(originallabel)) {
                     OWLClass matchedClass=(OWLClass)owlClassAndLabelsOfNewMatch.getKey(originallabel);
                     System.out.println("The matched class is " + matchedClass);
                     if(owlClassAndSubClassOfNewMatch.containsKey(matchedClass)) {
                         Set<OWLClass> subClass = owlClassAndSubClassOfNewMatch.get(matchedClass);
                         System.out.println("The subclass for "+matchedClass+" are : ");
                         System.out.println("The number of sub classes are "+subClass.size());
                         for(OWLClass cls:subClass)
                             System.out.println(cls);
                     }
                     if(owlClassAndSuperClassOfNewMatch.containsKey(matchedClass)) {
                         Set<OWLClass> superClass = owlClassAndSuperClassOfNewMatch.get(matchedClass);
                         System.out.println("The superclass for "+matchedClass+" are : ");
                         System.out.println("The number of super classes are "+superClass.size());
                         for(OWLClass cls:superClass)
                             System.out.println(cls);
                     }
                 }
             }
            System.out.println("The matches in VDOT are::::::::::::::::::::::::::::::::");
            ArrayList<String> list=entry.getValue();
            for(String s:list) {
                //System.out.println("sjkjashjdkahs");
                if (originalVSLemmatized.containsValue(s)) {
                    //System.out.println("Original and lemma " + s);
                    String originalValue = originalVSLemmatized.getKey(s).toString();
                    if (owlClassAndLabels.containsValue(originalValue)) {
                        OWLClass mainClass = (OWLClass) owlClassAndLabels.getKey(originalValue);
                        System.out.println("The class of VDOT is " + mainClass);
                    }
                }
            }

        }
    }

    public static void executeProcessAgain(TreeMap<OWLClass,Set<OWLClass>> maps) throws OWLOntologyCreationException, IOException {
        Iterator<Map.Entry<OWLClass, Set<OWLClass>>> it=maps.entrySet().iterator();
        System.out.println("The elements are(Combined) :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<OWLClass, Set<OWLClass>> entry = it.next();

            System.out.println("Key : "+entry.getKey());
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/CHEBI_23367>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/CHEBI_24432>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/CHEBI_24433>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/CHEBI_24835>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/CHEBI_24870>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/CHEBI_33250>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/CHEBI_33259>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/DOID_14566>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/MF_0000000>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/MF_0000020>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/OGMS_0000033>")) continue;
            if(entry.getKey().toString().contains("<http://www.ifomis.org/vdot/vdot_core.owl#C27351>")) continue;
            if(entry.getKey().toString().contains("<http://www.ifomis.org/vdot/vdot_core.owl#fma67513>")) continue;
            if(entry.getKey().toString().contains("<http://www.ifomis.org/vdot/vdot_core.owl#C14348>")) continue;
            if(entry.getKey().toString().contains("<http://www.ifomis.org/vdot/vdot_core.owl#fma70336>")) continue;
            if(entry.getKey().toString().contains("<http://www.ifomis.org/vdot/vdot_core.owl#C28133>")) continue;
            if(entry.getKey().toString().contains("<http://www.ifomis.org/vdot/vdot_core.owl#fma72300>")) continue;
            if(entry.getKey().toString().contains("<http://www.ifomis.org/vdot/vdot_core.owl#C14200>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/BFO_0000027>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/BFO_0000030>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/BFO_0000034>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/BFO_0000145>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/IAO_0000006>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/PATO_0000141>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/PATO_0001995>")) continue;
                if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/PATO_0002003>")) continue;
            if(entry.getKey().toString().contains("<http://purl.obolibrary.org/obo/BFO_0000040>")) continue;



            if(entry.getKey().toString().contains("fma")) continue;
            if(entry.getKey().toString().contains("CHEBI")) continue;
            if(entry.getKey().toString().contains("DOID")) continue;
            if(entry.getKey().toString().contains("GO")) continue;
            if(entry.getKey().toString().contains("OGMS")) continue;
            if(entry.getKey().toString().contains("HDOT")) continue;
            if(entry.getKey().toString().contains("IAO")) continue;
            if(entry.getKey().toString().contains("OBI")) continue;
            if(entry.getKey().toString().contains("OMRSE")) continue;
            Set<OWLClass> list=entry.getValue();
            for(OWLClass s:list) {
                IRI iri = s.getIRI();
                //OwlFileCreation(iri);
               classLabels(s);
                //fileModification();
            }
        }
    }
    public static void classLabels(OWLClass cls) throws OWLOntologyCreationException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        IRI iri = cls.getIRI();
        OWLOntology pizzaOntology = manager.loadOntologyFromOntologyDocument(iri);
        Set<OWLOntology> allOntologies = manager.getImportsClosure(pizzaOntology);
        System.out.println(allOntologies);
        OWLReasonerFactory reasonerFactory = new ElkReasonerFactory();
        OWLReasoner reasoner = reasonerFactory.createReasoner(pizzaOntology);
        //pizzaOntology
        OWLDataFactory factory = manager.getOWLDataFactory();

        Set<OWLClass> clazz = pizzaOntology.getClassesInSignature(Imports.INCLUDED);


        for (OWLClass ow : clazz) {

            if (ow.equals(cls)) {
                Set<OWLClass> subClasses = reasoner.getSubClasses(ow, true).getFlattened();
                owlClassAndSubClassOfNewMatch.put(ow, subClasses);
                Set<OWLClass> superclasses = reasoner.getSuperClasses(ow, true).getFlattened();
                owlClassAndSuperClassOfNewMatch.put(ow, superclasses);
                IRI cIRI = ow.getIRI();
                for (OWLOntology ontology : allOntologies) {
                    for (OWLAnnotationAssertionAxiom a : ontology.getAnnotationAssertionAxioms(cIRI)) {


                        if (a.getProperty().isLabel()) {
                            if (a.getValue() instanceof OWLLiteral) {
                                OWLLiteral val = (OWLLiteral) a.getValue();
                                //System.out.println(ow + " labelled " + val.getLiteral());
                                String v = val.getLiteral();
                                owlClassAndLabelsOfNewMatch.put(ow, v);
                                //FileUtils.writeStringToFile(file, v.toString().trim() + "\n", true);
                            }
                        }
                    }


                }
            }

        }
    }

    /**
     * Creates inverted Index for Vdot file
     * @throws IOException
     */
    public static void createInvertedIndex(BidiMap inputMap,TreeMap<String,ArrayList<String>> outputMap) throws IOException {
        //File vdotFile=new File("Final.txt");

        //LineIterator itVdot = FileUtils.lineIterator(vdotFile, "UTF-8");
        MapIterator itVdot=inputMap.mapIterator();
        while(itVdot.hasNext()){
            String original=itVdot.next().toString();
            String token=itVdot.getValue().toString();

                String splitArray[]=token.split(" ");
                for(String each:splitArray){
                    if(stopwords.contains(each)) continue;
                    //if(each.equals(" ")) continue;
                    if(outputMap.containsKey(each)){
                        ArrayList<String> list=outputMap.get(each);
                        if(!list.contains(token))
                            list.add(token);
                        outputMap.put(each,list);
                    }
                    else{
                        ArrayList<String> list=new ArrayList<String>();
                        list.add(token);
                        outputMap.put(each,list);
                    }
                }


        }




    }

    /**
     * Gets the sub and superclass from the TreeMap
     * @param map
     */
    public static void getClassValue(TreeMap<String,ArrayList<String>> map,TreeMap<OWLClass,Set<OWLClass>> subclass,TreeMap<OWLClass,Set<OWLClass>> superclass){
        Iterator<Map.Entry<String, ArrayList<String>>> it=map.entrySet().iterator();
       // System.out.println("The sub and super classes are ::::::::::::::::::::::");

        while (it.hasNext()) {
            Map.Entry<String, ArrayList<String>> entry = it.next();

           // System.out.println("Key : "+entry.getKey());
            ArrayList<String> list=entry.getValue();

            for(String s:list) {
                //System.out.println("sjkjashjdkahs");
                if(originalVSLemmatized.containsValue(s)) {
                    System.out.println("Original and lemma "+s);
                    String originalValue=originalVSLemmatized.getKey(s).toString();
                    if(owlClassAndLabels.containsValue(originalValue)) {
                        OWLClass mainClass = (OWLClass)owlClassAndLabels.getKey(originalValue);
                        System.out.println("The class of " + s + " is " + mainClass);
                        if(owlClassAndSubClass.containsKey(mainClass))
                        {
                            Set<OWLClass> subClasses=owlClassAndSubClass.get(mainClass);
                            System.out.println("The number of sub classes are "+subClasses.size());
                            for (OWLClass ow:subClasses) {
                                //System.out.println("The sub class of " + mainClass + " is " + ow);
                                if (subclass.containsKey(mainClass)) {
                                    for (Map.Entry<OWLClass, Set<OWLClass>> subIt : subclass.entrySet()) {
                                        OWLClass key = subIt.getKey();

                                        if(key.equals(mainClass)) {
                                            Set<OWLClass> value = subIt.getValue();
                                            value.add(ow);
                                            subclass.put(key,value);
                                        }
                                        // System.out.println(key + " => " + value);
                                    }

                                    //finalMatchowlClassAndSubClass.add(mainClass,)
                                }
                                else{
                                    Set<OWLClass> value=new HashSet<OWLClass>();
                                        value.add(ow);
                                    subclass.put(mainClass,value);
                                }
                            }
                        }
                        if(owlClassAndSuperClass.containsKey(mainClass))
                        {
                            Set<OWLClass> superClasses=owlClassAndSuperClass.get(mainClass);
                            System.out.println("The number of super classes are "+superClasses.size());
                            for (OWLClass ow:superClasses) {
                                //System.out.println("The super class of "+mainClass+" is "+ow);
                                if (superclass.containsKey(mainClass)) {
                                    for (Map.Entry<OWLClass, Set<OWLClass>> subIt : superclass.entrySet()) {
                                        OWLClass key = subIt.getKey();

                                        if(key.equals(mainClass)) {
                                            Set<OWLClass> value = subIt.getValue();
                                            value.add(ow);
                                            superclass.put(key,value);
                                        }
                                        // System.out.println(key + " => " + value);
                                    }

                                    //finalMatchowlClassAndSubClass.add(mainClass,)
                                }
                                else{
                                    Set<OWLClass> value=new HashSet<OWLClass>();
                                    value.add(ow);
                                    superclass.put(mainClass,value);
                                }
                            }
                        }
                    }
                    //System.out.print("Value " + s + "\n");
                }
            }
        }
    }

    /**
     * Compares the levenstein distance of the combined file with the keyset of inverted index
     * @throws IOException
     */
    public static  void compareWords(double per,String line,TreeMap<String,ArrayList<String>> invertedIndex,TreeMap<String,ArrayList<String>> resultMap) throws IOException {
        ArrayList<String> allWordsMatch=new ArrayList<String>();


                String splitArray[]=line.split(" ");
                for(String each:splitArray){
                    if(stopwords.contains(each)) continue;
                    Set<String> setOfKeys=invertedIndex.keySet();
                    for(String s:setOfKeys) {
                        double d = convertLevensteinToPercentage(each, s);
                        if (d >= per) {
                            allWordsMatch.addAll(invertedIndex.get(s));
                            if (resultMap.containsKey(line)) {
                                ArrayList<String> list = resultMap.get(line);
                                list.addAll(allWordsMatch);
                                resultMap.put(line, list);
                            } else {
                                resultMap.put(line, allWordsMatch);
                            }

                        }

                    }


        }

    }
    public static  void compareWordsRepo(double per) throws IOException {
        //File combined=new File("finalCombined.txt");
        //LineIterator itCombined = FileUtils.lineIterator(combined, "UTF-8");
        Iterator<Map.Entry<String, ArrayList<String>>> it=repoTermAndLemmaList.entrySet().iterator();
        while (it.hasNext()) {
            ArrayList<String> allWordsMatch=new ArrayList<String>();
            Map.Entry<String,ArrayList<String>> entry = it.next();

            String data=entry.getKey();
            ArrayList<String> list=entry.getValue();
            for(String eachLine:list) {
                //String splitArray[]=eachLine.split(" ");
                //for(String each:splitArray){
                //Compare only the last word as the most important word of the sentence nothing else
                String each=eachLine.substring(eachLine.lastIndexOf(" ")+1);
                    if(stopwords.contains(each)) continue;
                    Set<String> setOfKeys=vdotInvertedIndex.keySet();
                    for(String s:setOfKeys){
                        //System.out.println("Comparing between "+each+" from repo and "+s+" from vdot");
                        double d=convertLevensteinToPercentage(each,s);
                        if(d>=per){
                           // System.out.println("Comparing between "+each+" from repo and "+s+" from vdot for the word "+eachLine);
                            allWordsMatch.addAll(vdotInvertedIndex.get(s));
                            ArrayList<String> allWordsMatchRemoveDuplicate=new ArrayList<String>();
                            for(String word:allWordsMatch){
                                if(!allWordsMatchRemoveDuplicate.contains(word))
                                    allWordsMatchRemoveDuplicate.add(word);
                            }
                            repoLabelAndVDOTMatchList.put(eachLine,allWordsMatchRemoveDuplicate);
                            if(wordAndItsMatchForRepo.containsKey(data)){
                               // System.out.println(entry.getKey());

                                List<String> templist=wordAndItsMatchForRepo.get(data);
                                //BigList<String> biglist=new BigList<String>(templist);
                               // biglist.addAll(allWordsMatch);
                               // biglist.
                                for(String vdotWord:allWordsMatchRemoveDuplicate) {
                                    if(!templist.contains(vdotWord))
                                    templist.add(vdotWord);
                                }
                                wordAndItsMatchForRepo.put(data,templist);

                            }
                            else{
                                wordAndItsMatchForRepo.put(data,allWordsMatchRemoveDuplicate);
                           }

                        }
                    }
                //}
            }

            //System.out.println("Value : "+entry.getValue());
        }

            //allWordsMatch.clear();




    }
    public static void compareRepoNewAlgorithm(double per){
        //As discussed, I am changing the similarity algorithm a bit

        //First up, comapare if the search term  itself can give a per hit
        for(String searchWord:GetLabelsFromRepo.labels){
            ArrayList<String> allWordsMatch=new ArrayList<String>();
            Set<String> setOfKeys=vdotInvertedIndex.keySet();
            for(String s:setOfKeys) {
                //System.out.println("Comparing between "+each+" from repo and "+s+" from vdot");
                double d = convertLevensteinToPercentage(searchWord, s);
                if(d>=per){
                    // System.out.println("Comparing between "+each+" from repo and "+s+" from vdot for the word "+eachLine);
                    allWordsMatch.addAll(vdotInvertedIndex.get(s));
                    ArrayList<String> allWordsMatchRemoveDuplicate=new ArrayList<String>();
                    for(String word:allWordsMatch){
                        if(!allWordsMatchRemoveDuplicate.contains(word))
                            allWordsMatchRemoveDuplicate.add(word);
                    }
                    if(wordAndItsMatchForRepo.containsKey(searchWord)){
                        // System.out.println(entry.getKey());

                        List<String> templist=wordAndItsMatchForRepo.get(searchWord);
                        //BigList<String> biglist=new BigList<String>(templist);
                        // biglist.addAll(allWordsMatch);
                        // biglist.
                        for(String vdotWord:allWordsMatchRemoveDuplicate) {
                            if(!templist.contains(vdotWord))
                                templist.add(vdotWord);
                        }
                        wordAndItsMatchForRepo.put(searchWord,templist);
                    }
                    else{
                        wordAndItsMatchForRepo.put(searchWord,allWordsMatchRemoveDuplicate);
                    }
                }
            }
        }
    }
    public static  void compareWords(double per,BidiMap map) {
        wordAndItsMatchOfNewSet.clear();
        Iterator<Map.Entry<String, String>> it=map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();

            String original=entry.getKey();
            String line=entry.getValue();




            ArrayList<String> allWordsMatch=new ArrayList<String>();


            String splitArray[]=line.split(" ");
            for(String each:splitArray){
                if(stopwords.contains(each)) continue;
                Set<String> setOfKeys=vdotInvertedIndex.keySet();
                for(String s:setOfKeys){
                    double d=convertLevensteinToPercentage(each,s);
                    if(d>=per){
                        allWordsMatch.addAll(vdotInvertedIndex.get(s));
                        if(wordAndItsMatchOfNewSet.containsKey(line)){
                            ArrayList<String> list=wordAndItsMatchOfNewSet.get(line);
                            list.addAll(allWordsMatch);
                            wordAndItsMatchOfNewSet.put(line,list);
                        }
                        else{
                            wordAndItsMatchOfNewSet.put(line,allWordsMatch);
                        }

                    }

                }

            }
            //allWordsMatch.clear();


        }

    }
    public static void printOwlClassAndLabel(){
        Iterator<Map.Entry<OWLClass, String>> it=owlClassAndSynonym.entrySet().iterator();
        System.out.println("The elements are(Owl Class and Synonym) ::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<OWLClass,String> entry = it.next();

            System.out.println("Key : "+entry.getKey());
            System.out.println("Value : "+entry.getValue());
        }
    }

    public static void printVdotTerms(){
        Iterator<Map.Entry<String, ArrayList<String>>> it=vdotInvertedIndex.entrySet().iterator();
        System.out.println("The elements are(Vdot) ::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<String, ArrayList<String>> entry = it.next();

            System.out.println("Key : "+entry.getKey());
            ArrayList<String> list=entry.getValue();
            for(String s:list)
                System.out.print("Value "+s+"\n");
        }
    }
    public static void printTerms(TreeMap<String,ArrayList<String>> mapset){
        Iterator<Map.Entry<String, ArrayList<String>>> it=mapset.entrySet().iterator();
        System.out.println("The elements are(Combined) :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<String, ArrayList<String>> entry = it.next();

            System.out.println("Key : "+entry.getKey());
            ArrayList<String> list=entry.getValue();
            for(String s:list)
                System.out.print("Value "+s+"\n");
        }
    }
    public static Set<OWLClass> getOwlID(TreeMap<String,ArrayList<String>> mapset){
        Iterator<Map.Entry<String, ArrayList<String>>> it=mapset.entrySet().iterator();
Set<OWLClass> returnSet=new HashSet<OWLClass>();
        while (it.hasNext()) {
            Map.Entry<String, ArrayList<String>> entry = it.next();

            //System.out.println("Key : "+entry.getKey());
            ArrayList<String> list=entry.getValue();
            for(String s:list)
                if(originalVSLemmatized.containsValue(s)){
                String originalLabel=originalVSLemmatized.getKey(s).toString();
                if(owlClassAndLabels.containsValue(originalLabel)){
                    System.out.println("The term "+entry.getKey()+" matches for "+owlClassAndLabels.getKey(originalLabel));
                    returnSet.add((OWLClass)owlClassAndLabels.getKey(originalLabel));
                }
                }
        }
        return returnSet;
    }
    public static void saveOntologyToFile(IRI iri) throws OWLOntologyCreationException, FileNotFoundException, OWLOntologyStorageException {
        //Create the manager
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

//Load the ontology from the file
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(iri);
        File f=new File("savedOntology");
        OutputStream outputStream=new FileOutputStream(f);
        manager.saveOntology(ontology,outputStream);
    }
    public static void printTermsForRepo(TreeMap<String,List<String>> mapset){
        Iterator<Map.Entry<String, List<String>>> it=mapset.entrySet().iterator();
        System.out.println("The elements are(Combined) :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        while (it.hasNext()) {
            Map.Entry<String, List<String>> entry = it.next();

            System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Key:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: : "+entry.getKey());
            List<String> list=entry.getValue();
            for(String s:list)
                System.out.print("Value "+s+"\n");
        }
    }
    public static void addOWLClassToOntology(){

    }

    public static void printFinalChebiTerms(TreeMap<OWLClass,Set<OWLClass>> printMap){
        Iterator<Map.Entry<OWLClass, Set<OWLClass>>> it=printMap.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<OWLClass, Set<OWLClass>> entry = it.next();

            System.out.println("Key : "+entry.getKey());
            Set<OWLClass> list=entry.getValue();
            for(OWLClass s:list)
                System.out.print("Value "+s+"\n");
        }
    }


    public static void printOwlTerms(){
        MapIterator it=owlClassAndLabelsOfNewMatch.mapIterator();
        while(it.hasNext()){
            String key=it.next().toString();
            String value=it.getValue().toString();
            System.out.println(key +" :::::::::: "+ value );
        }
    }

    public static void printWordMatchTerms(){
        MapIterator it=owlClassAndLabels.mapIterator();
        while(it.hasNext()){
            String key=it.next().toString();
            String value=it.getValue().toString();
            System.out.println(key +" :::::::::: "+ value );
        }
    }

    public static void OwlFileCreation(IRI iri) throws OWLOntologyCreationException, IOException {
        //File file = new File("test.txt");
        owlClassAndLabels.clear();
        owlClassAndSuperClass.clear();
        owlClassAndSubClass.clear();
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        // Let's load an ontology from the web
       // IRI iri = IRI.create("http://www.ifomis.org/vdot/vdot_all.owl");
        OWLOntology pizzaOntology = manager.loadOntologyFromOntologyDocument(iri);
        Set<OWLOntology> allOntologies=manager.getImportsClosure(pizzaOntology);

        OWLReasonerFactory reasonerFactory = new ElkReasonerFactory();
        OWLReasoner reasoner = reasonerFactory.createReasoner(pizzaOntology);


        Set<OWLClass> clazz=pizzaOntology.getClassesInSignature(Imports.INCLUDED);
        Set<OWLAxiom> axiom= pizzaOntology.getAxioms(Imports.INCLUDED);

        for(OWLClass ow:clazz) {
            Set<OWLClass> subClasses = reasoner.getSubClasses(ow, true).getFlattened();
            owlClassAndSubClass.put(ow, subClasses);
            Set<OWLClass> superclasses = reasoner.getSuperClasses(ow, true).getFlattened();
            owlClassAndSuperClass.put(ow, superclasses);
            IRI cIRI = ow.getIRI();
            for (OWLOntology ontology : allOntologies) {
                for (OWLAnnotationAssertionAxiom a : ontology.getAnnotationAssertionAxioms(cIRI)) {


                    if (a.getProperty().isLabel()) {
                        if (a.getValue() instanceof OWLLiteral) {
                            OWLLiteral val = (OWLLiteral) a.getValue();
                            //System.out.println(ow + " labelled " + val.getLiteral());
                            String v = val.getLiteral();

                            owlClassAndLabels.put(ow, v);
                            //FileUtils.writeStringToFile(file, v.toString().trim() + "\n", true);
                        }
                    }
                }

                for (OWLAnnotationAssertionAxiom ax : ontology.getAxioms(AxiomType.ANNOTATION_ASSERTION)){
                    OWLAnnotationSubject subject = ax.getSubject();

                    if (ax.getProperty().toString().contains("hasSynonym")) {
                        String synonym=ax.getValue().toString();
                        owlClassAndSynonym.put(ow,synonym);
                    }
                }


            }
        }
   }

    public static void fileModification(BidiMap labelMap,BidiMap lemmaMap)  {

        MapIterator it=labelMap.mapIterator();
            while (it.hasNext()) {
                OWLClass cs=(OWLClass)it.next();

                String line = it.getValue().toString();
                //System.out.println(line);
                String original=line;
                line=line.toLowerCase();

                line=line.replaceAll("[^\\p{L}\\p{Nd}]+", " ");
                String lemmaString=lemmaOfWord(line);
                lemmaMap.put(original,lemmaString);
               // FileUtils.writeStringToFile(Finalfile, lemmaString+"\n",true);
            }


    }

    public static void lemmaChangeForRepositoryData(TreeMap<String,ArrayList<String>> repo){
        Iterator<Map.Entry<String, ArrayList<String>>> it=repo.entrySet().iterator();

       // System.out.println("The elements are(Owl Class and Label) ::::::::::::::::::::::");
        while (it.hasNext()) {
            ArrayList<String> newWordList=new ArrayList<String>();
            Map.Entry<String,ArrayList<String>> entry = it.next();

            //System.out.println("Key : "+entry.getKey());
            ArrayList<String> list=entry.getValue();
            for(String s:list){
                String lemmaString=lemmaOfWord(s);
                newWordList.add(lemmaString);
                originalVSLemmatizedForRepo.put(s,lemmaString);

            }
            repoTermAndLemmaList.put(entry.getKey(),newWordList);
               // System.out.println(s);
            //System.out.println("Value : "+entry.getValue());
        }

    }
    public static String lemmaOfWord(String documentText){
        documentText.trim();
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        StanfordCoreNLP pipeline;
        pipeline = new StanfordCoreNLP(props);
        StringBuilder lemmas = new StringBuilder();
        // Create an empty Annotation just with the given textd
        Annotation document = new Annotation(documentText);
        // run all Annotators on this text
        pipeline.annotate(document);
        // Iterate over all of the sentences found
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
                lemmas.append(" ");
                lemmas.append(token.get(CoreAnnotations.LemmaAnnotation.class));
            }
        }
        return lemmas.toString().trim();
    }
    public static void chebifileModification() throws IOException {
        Pattern regex = Pattern.compile("[^A-Za-z0-9]");
        File file = new File("CHEBI.txt");
        File Finalfile = new File("finalChebi.txt");
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        try{
            while (it.hasNext()) {
                String line = it.nextLine();
                line=line.toLowerCase();
                if(line.contains("\""))
                    line=line.replace("\"","");
                if(line.contains("@de"))
                    line=line.replace("@de","");
                if(line.contains("@en"))
                    line=line.replace("@en","");
                line=line.replaceAll("[\\W_]", " ");
                String lemmaString=lemmaOfWord(line);
                FileUtils.writeStringToFile(Finalfile, lemmaString+"\n",true);
            }

        }finally {
            LineIterator.closeQuietly(it);
        }
    }
    public static void genefileModification() throws IOException {
        Pattern regex = Pattern.compile("[^A-Za-z0-9]");
        File file = new File("Gene.txt");
        File Finalfile = new File("finalGene.txt");
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        try{
            while (it.hasNext()) {
                String line = it.nextLine();
                line=line.toLowerCase();
                if(line.contains("\""))
                    line=line.replace("\"","");
                if(line.contains("@de"))
                    line=line.replace("@de","");
                if(line.contains("@en"))
                    line=line.replace("@en","");
                line=line.replaceAll("[\\W_]", " ");
                String lemmaString=lemmaOfWord(line);
                FileUtils.writeStringToFile(Finalfile, lemmaString+"\n",true);
            }

        }finally {
            LineIterator.closeQuietly(it);
        }
    }
    public static void patofileModification() throws IOException {
        Pattern regex = Pattern.compile("[^A-Za-z0-9]");
        File file = new File("PATO");
        File Finalfile = new File("finalPato.txt");
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        try{
            while (it.hasNext()) {
                String line = it.nextLine();
                line=line.toLowerCase();
                if(line.contains("\""))
                    line=line.replace("\"","");
                if(line.contains("@de"))
                    line=line.replace("@de","");
                if(line.contains("@en"))
                    line=line.replace("@en","");
                line=line.replaceAll("[\\W_]", " ");
                String lemmaString=lemmaOfWord(line);
                FileUtils.writeStringToFile(Finalfile, lemmaString+"\n",true);
            }

        }finally {
            LineIterator.closeQuietly(it);
        }
    }
    public static void combinedfileModification() throws IOException {
        Pattern regex = Pattern.compile("[^A-Za-z0-9]");
        File file = new File("combined");
        File Finalfile = new File("finalCombined.txt");
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        try{
            while (it.hasNext()) {
                String line = it.nextLine();
                line=line.toLowerCase();
                if(line.contains("\""))
                    line=line.replace("\"","");
                if(line.contains("@de"))
                    line=line.replace("@de","");
                if(line.contains("@en"))
                    line=line.replace("@en","");
                line=line.replaceAll("[\\W_]", " ");
                String lemmaString=lemmaOfWord(line);
                FileUtils.writeStringToFile(Finalfile, lemmaString+"\n",true);
            }

        }finally {
            LineIterator.closeQuietly(it);
        }
    }
    /**
     * Generates a sorted Map of with the labels of PATO ontology
     *
     * @throws IOException - In case file is not found
     */
    public static void patoSimilarity(double percentage) throws IOException {
        System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::PATO SIMILARITY::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        File combined=new File("finalPato.txt");
        LineIterator itCombined = FileUtils.lineIterator(combined, "UTF-8");

        while (itCombined.hasNext()){
            ArrayList<String> allWordsMatch=new ArrayList<String>();
            String line=itCombined.nextLine();

            String splitArray[]=line.split(" ");
            for(String each:splitArray){
                if(stopwords.contains(each)) continue;
                Set<String> setOfKeys=vdotInvertedIndex.keySet();
                for(String s:setOfKeys){
                    double d=convertLevensteinToPercentage(each,s);
                    if(d>=percentage){
                        allWordsMatch.addAll(vdotInvertedIndex.get(s));
                        if(wordAndItsMatchPato.containsKey(line)){
                            ArrayList<String> list=wordAndItsMatchPato.get(line);
                            list.addAll(allWordsMatch);
                            wordAndItsMatchPato.put(line,list);
                        }
                        else{
                            wordAndItsMatchPato.put(line,allWordsMatch);
                        }

                    }

                }

            }
            //allWordsMatch.clear();


        }


    }

    /**
     * Generates a sorted Map of with the labels of Chebi ontology
     *
     * @throws IOException
     */
    public static void chebiSimilarity(double percentage) throws IOException {
        System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::CHEBI SIMILARITY::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        File combined=new File("finalChebi.txt");
        LineIterator itCombined = FileUtils.lineIterator(combined, "UTF-8");

        while (itCombined.hasNext()){
            ArrayList<String> allWordsMatch=new ArrayList<String>();
            String line=itCombined.nextLine();

            String splitArray[]=line.split(" ");
            for(String each:splitArray){
                if(stopwords.contains(each)) continue;
                Set<String> setOfKeys=vdotInvertedIndex.keySet();
                for(String s:setOfKeys){
                    double d=convertLevensteinToPercentage(each,s);
                    if(d>=percentage){
                        allWordsMatch.addAll(vdotInvertedIndex.get(s));
                        if(wordAndItsMatchChebi.containsKey(line)){
                            ArrayList<String> list=wordAndItsMatchChebi.get(line);
                            list.addAll(allWordsMatch);
                            wordAndItsMatchChebi.put(line,list);
                        }
                        else{
                            wordAndItsMatchChebi.put(line,allWordsMatch);
                        }

                    }

                }

            }
            //allWordsMatch.clear();


        }

    }

    /**
     * Generates a sorted Map of with the labels of Gene ontology
     *
     * @throws IOException
     */
    public static void geneSimilarity(double percentage) throws IOException {
        System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::GENE SIMILARITY::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        File combined=new File("finalGene.txt");
        LineIterator itCombined = FileUtils.lineIterator(combined, "UTF-8");

        while (itCombined.hasNext()){
            ArrayList<String> allWordsMatch=new ArrayList<String>();
            String line=itCombined.nextLine();

            String splitArray[]=line.split(" ");
            for(String each:splitArray){
                if(stopwords.contains(each)) continue;
                Set<String> setOfKeys=vdotInvertedIndex.keySet();
                for(String s:setOfKeys){
                    double d=convertLevensteinToPercentage(each,s);
                    if(d>=percentage){
                        allWordsMatch.addAll(vdotInvertedIndex.get(s));
                        if(wordAndItsMatchGene.containsKey(line)){
                            ArrayList<String> list=wordAndItsMatchGene.get(line);
                            list.addAll(allWordsMatch);
                            wordAndItsMatchGene.put(line,list);
                        }
                        else{
                            wordAndItsMatchGene.put(line,allWordsMatch);
                        }

                    }

                }

            }
            //allWordsMatch.clear();


        }
    }

    /**
     * Calculates the levenstein distance between 2 words and converts to percentage
     * @param searchWord
     * @param owlWord - Label
     * @return percentage of similarity
     */
    public static double convertLevensteinToPercentage(String searchWord,String owlWord){
        // Refer to https://www.linkedin.com/pulse/percentage-string-match-levenshtein-distance-jacob-parr for algorithm
        Levenshtein l = new Levenshtein();//Function for levenshtein from info.debatty.java.stringsimilarity. ; apache commans can be used as well
        double len=Math.max(searchWord.length(),owlWord.length());
        double distance=len-l.distance(searchWord, owlWord);
        if(distance==0)
            return 0;
        double percentage=(distance/len)*100;
        return percentage;
    }
    public static void combinedSimilarity(double percentage) throws IOException
    {
        System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::ALL SIMILARITY::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        TreeMap<String,Double> levenshteinDistance=new TreeMap<String, Double>();//Create a TreeMap with Key - the label in Pato; and Value-Percentage of similarity
        File file = new File("combined");
        File vdotFile=new File("Final.txt");
        List lines = FileUtils.readLines(file, "UTF-8");
        //Iterate through each line and compute the levenstein distance
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        LineIterator vdotIt=FileUtils.lineIterator(vdotFile,"UTF-8");
        try {
            while(vdotIt.hasNext()) {
                String vdotLine=vdotIt.nextLine();

                while (it.hasNext()) {
                    String line = it.nextLine();
                    //Reads a line(label) and calls the convertToPercentage method which will return the similarity percentage
                    double per = convertLevensteinToPercentage(line, vdotLine);
                    levenshteinDistance.put(line, per);
                }
                //Sort the treeMap by value
                Map<String,Double> sortedMap= MapUtil.sortByValue(levenshteinDistance);
                //Display the Map value
                Set set = sortedMap.entrySet();
                // Get an iterator
                Iterator i = set.iterator();
                // Display elements
                //int count=0;
                while(i.hasNext()) {
                    Map.Entry me = (Map.Entry)i.next();
                    Double v=(Double)me.getValue();
                    if(v>percentage)
                        System.out.println("The levenshtein distance between "+vdotLine+" and "+ me.getKey()+" is : "+ me.getValue());

                }
            }
        } finally {
            LineIterator.closeQuietly(it);
        }
    }

}
