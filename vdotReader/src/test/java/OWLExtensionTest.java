import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import java.io.IOException;

import static org.junit.Assert.*;

public class OWLExtensionTest {

    @Test
    public void getLatestVersionNumber() throws IOException {
       // OWLExtension owlExtension = new OWLExtension();
      //  assertEquals(3,owlExtension.getLatestVersionNumber());
    }

    @Test
    public void keepTrackofVersion() throws IOException {
        //OWLExtension owlExtension = new OWLExtension();
        //owlExtension.keepTrackofVersion(1);
    }

    @Test
    public void extend() throws OWLOntologyCreationException, OWLOntologyStorageException, IOException {
        //OWLExtension owlExtension = new OWLExtension();
        //owlExtension.extend("sourceontology.owl","http://owl.mynewontology.example#person","http://purl.obolibrary.org/obo/GO_0050878");

    }

    @Test
    public void uploadToGit() throws OWLOntologyCreationException, OWLOntologyStorageException, IOException, GitAPIException {
        OWLExtension owlExtension = new OWLExtension();
        String fileName = owlExtension.extend("sourceontology.owl","http://owl.mynewontology.example#person","http://purl.obolibrary.org/obo/GO_0050878");
        owlExtension.uploadToGit(fileName,"curator");
    }

    @Test
    public void usingBufferedReader() {
        //OWLExtension owlExtension = new OWLExtension();
        //String line = owlExtension.usingBufferedReader("VDOT_ALL_2.owl");
        //System.out.println(line);
    }
}