package owlextesiontest;

import org.junit.Test;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.semanticweb.owlapi.model.*;
import owlextension.OWLClassTools;
import owlextension.OntologyTools;

import java.io.File;
import java.util.Set;


public class OWLClassToolsTest {

    @Test
    public void subClass() throws OWLOntologyCreationException {
        OWLOntology ontology = new OntologyTools().loadOntology(new File("sourceontology.owl"));
        OWLDataFactory df = ontology.getOWLOntologyManager().getOWLDataFactory();
        OWLClass class_ = df.getOWLClass(IRI.create("http://purl.obolibrary.org/obo/GO_0098801"));
        Set<OWLClass> subclasses = new OWLClassTools(ontology,class_).subClass();
        System.out.println(subclasses);


    }
    @Test
    public void superClass() throws OWLOntologyCreationException {
        OWLOntology ontology = new OntologyTools().loadOntology(new File("sourceontology.owl"));
        OWLDataFactory df = ontology.getOWLOntologyManager().getOWLDataFactory();
        OWLClass class_ = df.getOWLClass(IRI.create("http://purl.obolibrary.org/obo/GO_1904318"));
        Set<OWLClass> superlasses = new OWLClassTools(ontology,class_).superClass();
        System.out.println(superlasses);
    }
    @Test
    public void getLabel(){
        String OWLresource = "<rdf:RDF    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"    xmlns:obo=\"http://purl.obolibrary.org/obo/\"    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"    xmlns:oboInOwl=\"http://www.geneontology.org/formats/oboInOwl#\"    xmlns:skos=\"http://www.w3.org/2004/02/skos/core#\"    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\">  <owl:Class rdf:about=\"http://purl.obolibrary.org/obo/GO_1903322\">    <oboInOwl:id>GO:1903322</oboInOwl:id>    <owl:equivalentClass>      <owl:Class>        <owl:intersectionOf rdf:parseType=\"Collection\">          <owl:Restriction>            <owl:someValuesFrom rdf:resource=\"http://purl.obolibrary.org/obo/GO_0070647\"/>            <owl:onProperty rdf:resource=\"http://purl.obolibrary.org/obo/RO_0002213\"/>          </owl:Restriction>          <rdf:Description rdf:about=\"http://purl.obolibrary.org/obo/GO_0008150\"/>        </owl:intersectionOf>      </owl:Class>    </owl:equivalentClass>    <rdfs:subClassOf rdf:resource=\"http://purl.obolibrary.org/obo/GO_1903320\"/>    <oboInOwl:hasOBONamespace>biological_process</oboInOwl:hasOBONamespace>    <rdfs:subClassOf>      <owl:Restriction>        <owl:someValuesFrom rdf:resource=\"http://purl.obolibrary.org/obo/GO_0070647\"/>        <owl:onProperty rdf:resource=\"http://purl.obolibrary.org/obo/RO_0002213\"/>      </owl:Restriction>    </rdfs:subClassOf>    <obo:IAO_0000115>Any process that activates or increases the frequency, rate or extent of protein modification by small protein conjugation or removal.</obo:IAO_0000115>    <oboInOwl:created_by>vw</oboInOwl:created_by>    <oboInOwl:hasNarrowSynonym>activation of protein modification by small protein conjugation or removal</oboInOwl:hasNarrowSynonym>    <oboInOwl:hasExactSynonym>up-regulation of protein modification by small protein conjugation or removal</oboInOwl:hasExactSynonym>    <oboInOwl:hasExactSynonym>up regulation of protein modification by small protein conjugation or removal</oboInOwl:hasExactSynonym>    <rdfs:subClassOf rdf:resource=\"http://purl.obolibrary.org/obo/GO_0031401\"/>    <oboInOwl:creation_date>2014-08-18T13:01:15Z</oboInOwl:creation_date>    <oboInOwl:hasExactSynonym>upregulation of protein modification by small protein conjugation or removal</oboInOwl:hasExactSynonym>    <rdfs:label>positive regulation of protein modification by small protein conjugation or removal</rdfs:label>  </owl:Class></rdf:RDF>";
        OWLClassTools tools = new OWLClassTools(OWLresource);
        System.out.println(tools.getLabel());
    }
}