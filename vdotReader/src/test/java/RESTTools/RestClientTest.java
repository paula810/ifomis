package RESTTools;

import RepoAccessTools.RetriveData;
import org.junit.Test;

import static org.junit.Assert.*;

public class RestClientTest {

    @Test
    public void post() {
        String REST_URL = "http://xploit.ibmt.fraunhofer.de:8080/authorization-service/oauth/token?grant_type=password&username=s8mdkadi&password=CZ094gh401818898982!";
        String userCredentials = "xploit-trusted-client:Xpl0it!";
        RestClient restClient = new RestClient();
        System.out.println(restClient.post(REST_URL,userCredentials));
    }
    @Test
    public  void get(){
        RetriveData dataRetrive = new RetriveData();
        String accesstoken = dataRetrive.getAccessToken();
        String REST_URL = "http://xploit.ibmt.fraunhofer.de:8080/portal/rest/semanticRepository/queryClass?search=http://purl.obolibrary.org/obo/GO_1903322";//"http://xploit.ibmt.fraunhofer.de:8080/portal/rest/semanticRepository" + "/query?search="+ "mating";
        RestClient restClient = new RestClient();
        System.out.println(restClient.get(REST_URL,accesstoken));
    }
}