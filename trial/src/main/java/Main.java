import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.*;

import info.debatty.java.stringsimilarity.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter a search word");
        String searchWord=sc.nextLine();
        System.out.println("Similarity with PATO is :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        patoSimilarity(searchWord);
        System.out.println("Similarity with CHEBI is ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        chebiSimilarity(searchWord);
        System.out.println("Similarity with Gene is :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        geneSimilarity(searchWord);
    }

    /**
     * Generates a sorted Map of with the labels of PATO ontology
     * @param s - Search word
     * @throws IOException - In case file is not found
     */
    public static void patoSimilarity(String s) throws IOException {

        TreeMap<String,Double> levenshteinDistance=new TreeMap<String, Double>();//Create a TreeMap with Key - the label in Pato; and Value-Percentage of similarity
        File file = new File("/home/pallavi/Hiwi/ifomis/trial/PATO");
        List lines = FileUtils.readLines(file, "UTF-8");
        //Iterate through each line and compute the levenstein distance
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        try {
            while (it.hasNext()) {
                String line = it.nextLine();
                //Reads a line(label) and calls the convertToPercentage method which will return the similarity percentage
                double per=convertLevensteinToPercentage(s,line);
                levenshteinDistance.put(line,per);
            }
        } finally {
            LineIterator.closeQuietly(it);
        }
        //Sort the treeMap by value
       Map<String,Double> sortedMap= MapUtil.sortByValue(levenshteinDistance);
        //Display the Map value
        Set set = sortedMap.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        // Display elements
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
             System.out.println("The levenshtein distance between "+s+" and "+ me.getKey()+" is : "+ me.getValue());

        }

    }

    /**
     * Generates a sorted Map of with the labels of Chebi ontology
     * @param s
     * @throws IOException
     */
    public static void chebiSimilarity(String s) throws IOException {
       // Levenshtein l = new Levenshtein();
        File file = new File( "/home/pallavi/Hiwi/ifomis/trial/CHEBI.txt");
        List lines = FileUtils.readLines(file, "UTF-8");
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        TreeMap<String,Double> levenshteinDistance=new TreeMap<String, Double>();
        try {
            while (it.hasNext()) {
                String line = it.nextLine();
                double per=convertLevensteinToPercentage(s,line);
                levenshteinDistance.put(line,per);


            }
        } finally {
            LineIterator.closeQuietly(it);
        }
        Map<String,Double> sortedMap= MapUtil.sortByValue(levenshteinDistance);
        Set set = sortedMap.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        // Display elements
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            System.out.println("The levenshtein distance between "+s+" and "+ me.getKey()+" is : "+ me.getValue());

        }
    }

    /**
     * Generates a sorted Map of with the labels of Gene ontology
     * @param s
     * @throws IOException
     */
    public static void geneSimilarity(String s) throws IOException {
// Same as Pato only the ontology changes
        TreeMap<String,Double> levenshteinDistance=new TreeMap<String, Double>();
        File file = new File("/home/pallavi/Hiwi/ifomis/trial/Gene.txt");
        List lines = FileUtils.readLines(file, "UTF-8");
        LineIterator it = FileUtils.lineIterator(file, "UTF-8");
        try {
            while (it.hasNext()) {
                String line = it.nextLine();
                double per=convertLevensteinToPercentage(s,line);
                levenshteinDistance.put(line,per);


            }
        } finally {
            LineIterator.closeQuietly(it);
            //file.close();
        }
        Map<String,Double> sortedMap= MapUtil.sortByValue(levenshteinDistance);
        Set set = sortedMap.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        // Display elements
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            System.out.println("The levenshtein distance between "+s+" and "+ me.getKey()+" is : "+ me.getValue());

        }

    }

    /**
     * Calculates the levenstein distance between 2 words and converts to percentage
     * @param searchWord
     * @param owlWord - Label
     * @return percentage of similarity
     */
    public static double convertLevensteinToPercentage(String searchWord,String owlWord){
        // Refer to https://www.linkedin.com/pulse/percentage-string-match-levenshtein-distance-jacob-parr for algorithm
        Levenshtein l = new Levenshtein();//Function for levenshtein from info.debatty.java.stringsimilarity. ; apache commans can be used as well
        double len=Math.max(searchWord.length(),owlWord.length());
        double distance=len-l.distance(searchWord, owlWord);
        if(distance==0)
            return 0;
        double percentage=(distance/len)*100;
        return percentage;
    }


}
