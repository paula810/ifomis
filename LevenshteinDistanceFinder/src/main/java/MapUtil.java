/**
 * Created by pallavi on 2/16/17.
 */
import java.util.*;

public class MapUtil
{
   //Refer to http://stackoverflow.com/questions/109383/sort-a-mapkey-value-by-values-java for algorithm
   public static <K, V extends Comparable<? super V>> Map<K, V>
   sortByValue( Map<K, V> map )
   {
      List<Map.Entry<K, V>> list =
         new LinkedList<Map.Entry<K, V>>( map.entrySet() );
      Collections.sort( list, new Comparator<Map.Entry<K, V>>()
      {
         public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
         {
            return (o2.getValue()).compareTo(o1.getValue());
         }
      } );

      Map<K, V> result = new LinkedHashMap<K, V>();
      for (Map.Entry<K, V> entry : list)
      {
         result.put( entry.getKey(), entry.getValue() );
      }
      return result;
   }
}
