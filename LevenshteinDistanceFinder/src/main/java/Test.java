import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by maktareq on 18.02.17.
 */
public class Test {
   public static void main(String[]args){
      Scanner scan=new Scanner(System.in);
      System.out.println("Give the directory name");
      String directory=scan.nextLine();
      System.out.println("Minimum acceptable percentage");
      double levensDistPercent=scan.nextDouble();
      System.out.println("Give the Label");
      String givenLabel=scan.nextLine();
      String givenLabell=scan.nextLine();



      OntologyLabel label=new OntologyLabel(directory);
      TreeMap<String,Double> levenshteinDistance=new TreeMap<String, Double>();

      try {

         List<String> allLabels=label.getOntologyLabel();
         for(int i=0;i<allLabels.size();i++){
            DistenceCalculator distcal=new DistenceCalculator();

            double per=distcal.convertLevensteinToPercentage(givenLabell,allLabels.get(i));
            if(per>=levensDistPercent) {
               levenshteinDistance.put(allLabels.get(i), per);
            }

            //System.out.println(allLabels.get(i));

         }
      } catch (IOException e) {
         e.printStackTrace();
      }
      Map<String,Double> sortedMap= MapUtil.sortByValue(levenshteinDistance);
      Set set = sortedMap.entrySet();
      // Get an iterator
      Iterator i = set.iterator();
      // Display elements
      while(i.hasNext()) {
         Map.Entry me = (Map.Entry)i.next();


         System.out.println("The levenshtein distance between "+givenLabell+" and "+ me.getKey()+" is : "+ me.getValue());

      }

   }
}
