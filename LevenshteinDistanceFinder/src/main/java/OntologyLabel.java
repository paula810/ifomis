/**
 * Created by maktareq on 18.02.17.
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class OntologyLabel {
   private String directory=null;
   Logger report= Logger.getLogger(OntologyLabel.class.getName());
   BufferedReader insert=null;
   public OntologyLabel(String path){
      directory=path;
   }
   public List<String> getOntologyLabel() throws IOException {
      List<String> ontologyLabel= null;
      ontologyLabel = new ArrayList<String>();
      try {
         insert= new BufferedReader(new FileReader(directory));
         String testString;
         while((testString=insert.readLine())!=null) {
            ontologyLabel.add(testString);
         }
      }
      catch (FileNotFoundException fe) {
         report.info("File Not Found"+fe.getMessage());
      }
      catch (IOException io) {
         report.info("IOException"+io.getMessage());
      }
      finally {
         if(insert!=null){
            insert.close();

         }
      }
      return ontologyLabel;
   }

}
